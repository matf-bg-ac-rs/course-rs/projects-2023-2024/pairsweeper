#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "../../../src/server/pairsweeper_encryption_handler.hpp"

TEST_CASE("TEST ENCRYPTION") {
   SECTION("ENCRYPTION AND DECRYPTION TEST") {
       // ARRANGE
       PairsweeperEncryptionHandler* encHandler = PairsweeperEncryptionHandler::getInstance();
       encHandler->generateKey("16CharactersOverAndOver");

       // ACT
       std::string plaintext = "Hello, World!";
       std::string ciphertext = encHandler->encrypt(plaintext);
       std::string decryptedText = encHandler->decrypt(ciphertext);

       // ASSERT
       REQUIRE(decryptedText == plaintext);
   }
    SECTION("ENCRYPTION WITH EMPTY STRING") {
      // ARRANGE
      PairsweeperEncryptionHandler* encHandler = PairsweeperEncryptionHandler::getInstance();
      encHandler->setKey("16CharactersOverAndOver");

      // ACT
      std::string plaintext = "";
      std::string ciphertext = encHandler->encrypt(plaintext);
      std::string decryptedText = encHandler->decrypt(ciphertext);

      // ASSERT
      REQUIRE(decryptedText == plaintext);
  }

  SECTION("ENCRYPTION WITH NON-ASCII CHARACTERS") {
      // ARRANGE
      PairsweeperEncryptionHandler* encHandler = PairsweeperEncryptionHandler::getInstance();
      encHandler->setKey("16CharactersOverAndOver");

      // ACT
      std::string plaintext = "Hello, 世界!";
      std::string ciphertext = encHandler->encrypt(plaintext);
      std::string decryptedText = encHandler->decrypt(ciphertext);

      // ASSERT
      REQUIRE(decryptedText == plaintext);
  }
}
