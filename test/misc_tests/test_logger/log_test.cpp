#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../../../src/server/pairsweeper_logger.hpp"


TEST_CASE("TEST LOGGER") {
  SECTION("LOG INFO MESSAGE") {
      // ARRANGE
      std::string message = "Info Message";

      // ACT
      Logger::info(message);

      // ASSERT
      // Rucno
  }

  SECTION("LOG ERROR MESSAGE") {
      // ARRANGE
      std::string message = "Error Message";

      // ACT
      Logger::error(message);

      // ASSERT
      // Rucno
  }
}
