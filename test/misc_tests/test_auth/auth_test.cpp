#define CATCH_CONFIG_MAIN
#include"catch.hpp"

#include"../../../src/server/pairsweeper_authentication_handler.hpp"

TEST_CASE("TEST AUTH") {
   SECTION("AUTH REGISTER AND VALIDATE TEST") {
       // ARRANGE
       PairsweeperAuthenticationHandler* authHandler = PairsweeperAuthenticationHandler::getInstance();

       // ACT
       authHandler->registerUser("DemoUser", "DemoPasswordHash");

       // ASSERT
       REQUIRE(authHandler->validateUser("DemoUser", "DemoPasswordHash") == true);
   }

   SECTION("AUTH INVALID USERNAME TEST") {
       // ARRANGE
       PairsweeperAuthenticationHandler* authHandler = PairsweeperAuthenticationHandler::getInstance();

       // ACT
       bool result = authHandler->validateUser("InvalidUser", "DemoPasswordHash");

       // ASSERT
       REQUIRE(result == false);
   }

   SECTION("AUTH INVALID PASSWORD TEST") {
       // ARRANGE
       PairsweeperAuthenticationHandler* authHandler = PairsweeperAuthenticationHandler::getInstance();
       authHandler->registerUser("DemoUser", "DemoPasswordHash");

       // ACT
       bool result = authHandler->validateUser("DemoUser", "InvalidPasswordHash");

       // ASSERT
       REQUIRE(result == false);
   }
}
