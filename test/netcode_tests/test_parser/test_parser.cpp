#define CATCH_CONFIG_MAIN
#include "../catch.hpp"
#include "packet_parser.hpp"
#include "../../../src/netcode/packet_parser.hpp"
#include "../../../src/netcode/packet_match_start_response.hpp"
#include "../../../src/netcode/packet_login_request.hpp"


TEST_CASE("PacketParser parse_inbound_packet login_request", "[PacketParser]") {
 // Arrange
 std::string jsonstring = R"({"type": "login_request", "username": "testUser", "hash": "testHash", "version": "testVersion"})";

 // Act
 PacketRequest* request = PacketParser::parse_inbound_packet(jsonstring);

 // Assert
 REQUIRE(dynamic_cast<PacketLoginRequest*>(request) != nullptr);

 REQUIRE(request->getType() == "login_request");

 delete request;
}

TEST_CASE("PacketParser parse_inbound_packet invalid type", "[PacketParser]") {
 // Arrange
 std::string jsonstring = R"({"type": "invalid_type"})";

 // Act
 PacketRequest* request = PacketParser::parse_inbound_packet(jsonstring);

 // Assert
 REQUIRE(request == nullptr);
}

TEST_CASE("PacketParser parse_response_packet match_start_response", "[PacketParser]") {
 // Arrange
 std::string jsonstring = R"({"type": "match_start_response", "opponentName": "testOpponent"})";

 // Act
 PacketResponse* response = PacketParser::parse_response_packet(jsonstring);

 // Assert
 REQUIRE(dynamic_cast<PacketMatchStartResponse*>(response) != nullptr);
 REQUIRE(response->getType() == "match_start_response");

 delete response;
}
