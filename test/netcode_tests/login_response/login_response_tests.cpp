#define CATCH_CONFIG_MAIN
#include "../catch.hpp"
#include "packet_login_response.hpp"

TEST_CASE("PacketLoginResponse creation", "[PacketLoginResponse]") {
 // Arrange

 // Act
 PacketLoginResponse response;

 // Assert
}

TEST_CASE("PacketLoginResponse toJson", "[PacketLoginResponse]") {
 // Arrange
 PacketLoginResponse response;

 // Act
 std::string json = response.toJson();

 // Assert
 REQUIRE(!json.empty());
 REQUIRE(json.find("\"type\":\"login_response\"") != std::string::npos);
}

TEST_CASE("PacketLoginResponse fromJson", "[PacketLoginResponse]") {
 // Arrange
 PacketLoginResponse response;
 std::string json = response.toJson();

 // Act
 PacketLoginResponse* newResponse = PacketLoginResponse::fromJson(json);

 // Assert
 REQUIRE(newResponse != nullptr);

 delete newResponse;
}
