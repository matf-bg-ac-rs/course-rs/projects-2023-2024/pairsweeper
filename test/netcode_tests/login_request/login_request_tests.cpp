#define CATCH_CONFIG_MAIN
#include "../catch.hpp"
#include "packet_login_request.hpp"

TEST_CASE("PacketLoginRequest creation", "[PacketLoginRequest]") {
 // Arrange
 std::string username = "testUser";
 std::string hash = "testHash";
 std::string version = "testVersion";

 // Act
 PacketLoginRequest request(username, hash, version);

 // Assert
 REQUIRE(request.get_username() == username);
 REQUIRE(request.getHash() == hash);
 REQUIRE(request.getVersion() == version);
}

TEST_CASE("PacketLoginRequest toJson", "[PacketLoginRequest]") {
 // Arrange
 std::string username = "testUser";
 std::string hash = "testHash";
 std::string version = "testVersion";
 PacketLoginRequest request(username, hash, version);

 // Act
 std::string json = request.toJson();

 // Assert
 REQUIRE(!json.empty());
 REQUIRE(json.find("\"type\":\"login_request\"") != std::string::npos);
 REQUIRE(json.find("\"username\":\"" + username + "\"") != std::string::npos);
 REQUIRE(json.find("\"hash\":\"" + hash + "\"") != std::string::npos);
 REQUIRE(json.find("\"version\":\"" + version + "\"") != std::string::npos);
}

TEST_CASE("PacketLoginRequest fromJson", "[PacketLoginRequest]") {
 // Arrange
 std::string username = "testUser";
 std::string hash = "testHash";
 std::string version = "testVersion";
 PacketLoginRequest request(username, hash, version);
 std::string json = request.toJson();

 // Act
 PacketLoginRequest* newRequest = PacketLoginRequest::fromJson(json);

 // Assert
 REQUIRE(newRequest->get_username() == username);
 REQUIRE(newRequest->getHash() == hash);
 REQUIRE(newRequest->getVersion() == version);

 delete newRequest;
}
