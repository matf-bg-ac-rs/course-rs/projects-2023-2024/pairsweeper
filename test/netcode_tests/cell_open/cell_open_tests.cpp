#define CATCH_CONFIG_MAIN
#include "../catch.hpp"
#include "../../../src/netcode/packet_cell_open_request.hpp"

TEST_CASE("PacketCellOpenRequest creation", "[PacketCellOpenRequest]") {
  // Arrange
  int row = 1;
  int column = -24;

  // Act
  PacketCellOpenRequest request(row, column);

  // Assert
  REQUIRE(request.get_row() == row);
  REQUIRE(request.get_column() == column);
}

TEST_CASE("PacketCellOpenRequest toJson", "[PacketCellOpenRequest]") {
  // Arrange
  int row = 4;
  int column = 5;
  PacketCellOpenRequest request(row, column);

  // Act
  std::string json = request.toJson();

  // Assert
  REQUIRE(!json.empty());
  REQUIRE(json.find("\"type\":\"cell_open_request\"") != std::string::npos);
  REQUIRE(json.find("\"row\":" + std::to_string(row)) != std::string::npos);
  REQUIRE(json.find("\"column\":" + std::to_string(column)) != std::string::npos);
}

TEST_CASE("PacketCellOpenRequest fromJson", "[PacketCellOpenRequest]") {
  // Arrange
  int row = 1;
  int column = 2;
  PacketCellOpenRequest request(row, column);
  std::string json = request.toJson();

  // Act
  PacketCellOpenRequest* newRequest = PacketCellOpenRequest::fromJson(json);

  // Assert
  REQUIRE(newRequest->get_row() == row);
  REQUIRE(newRequest->get_column() == column);

  delete newRequest;
}
