#define CATCH_CONFIG_MAIN
#include"catch.hpp"

#include"../../../src/common/cell.hpp"

class TestCell : public Cell { // ovo ovako mora jer Cell ima samo virtualne metode 
public:
    TestCell(CellState cellState) : Cell(cellState) {}

    CellState getCellState() const override {
        return _cellState;
    }

    void setCellState(CellState cellState) override {
        _cellState = cellState;
    }
};

TEST_CASE("TEST CELL") {
    SECTION("Test constructor") {
        // ARRANGE
        TestCell cell(CellState::COVERED);

        // ACT
        CellState currentState = cell.getCellState();
        
        //ASSERT
        REQUIRE(currentState == CellState::COVERED);
    }

    SECTION("Test setting and getting state") {
        // ARRANGE
        TestCell cell(CellState::COVERED);
        // ACT 
        cell.setCellState(CellState::UNCOVERED);
        CellState newState = cell.getCellState();

        // ASSERT
        REQUIRE(newState == CellState::UNCOVERED);

        //ACT
        cell.setCellState(CellState::FLAGGED);
        CellState flaggedState = cell.getCellState();
        
        //ASSERT
        REQUIRE(flaggedState == CellState::FLAGGED);

    }
}