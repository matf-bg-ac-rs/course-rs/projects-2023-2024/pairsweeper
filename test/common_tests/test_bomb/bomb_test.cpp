#define CATCH_CONFIG_MAIN
#include"catch.hpp"

#include"../../../src/common/cell_bomb.hpp"

TEST_CASE("TEST BOMB") {
    SECTION("Test setters and getters") {
        // ARRANGE
        CellBomb bomb(CellState::COVERED);

        // ACT
        bomb.setCellState(CellState::FLAGGED);
        CellState currentState = bomb.getCellState();

        // ASSERT
        REQUIRE(currentState == CellState::FLAGGED);
    }
}
