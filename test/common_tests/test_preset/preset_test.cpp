#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "../../../src/common/game_preset.hpp"

TEST_CASE("TEST PRESET") {
    SECTION("Test getting rows, cols, and numMines") {
        // ARRANGE
        GamePreset preset(5, 5, 10, gameMode::SINGLEPLAYER, difficulty::EASY);

        // ACT
        int rows = preset.getRows();
        int cols = preset.getCols();
        int numMines = preset.getNumMines();

        // ASSERT
        REQUIRE(rows == 5);
        REQUIRE(cols == 5);
        REQUIRE(numMines == 10);
    }

    SECTION("Test getting game mode") {
        // ARRANGE
        GamePreset preset(5, 5, 10, gameMode::COOP, difficulty::HARD);

        // ACT
        int mode = preset.getGameMode();

        // ASSERT
        REQUIRE(mode == gameMode::COOP);
    }


}
