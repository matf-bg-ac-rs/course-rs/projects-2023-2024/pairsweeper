#define CATCH_CONFIG_MAIN
#include"catch.hpp"

#include"../../../src/common/cell_board.hpp"

TEST_CASE("TEST BOARD") {
    SECTION("Test getting cell from position") {
        // ARRANGE
        CellBoard board(3, 3);

        // ACT
        board.setCellToIndicator(1,1, CellState::COVERED, 1);// ovo mora jer smo postavili da se inicijalizuje na nullptr
        Cell* cell = board.getCellFromPosition(1, 1);
        

        // ASSERT
        REQUIRE(cell != nullptr);
        delete cell;
    }   

    SECTION("Test nearby bomb count from position") {
        // ARRANGE
        CellBoard board(3, 3);
        board.setCellToBomb(0, 0, CellState::UNCOVERED);
        board.setCellToBomb(1, 0, CellState::UNCOVERED);

        // ACT
        int bombCount = board.getNearbyBombCountFromPosition(1, 1);

        // ASSERT
        REQUIRE(bombCount == 2);
    }

    SECTION("Test constructor and getters"){
        //ARRANGE
        CellBoard myBoard(3,4);
       
        //ACT
        ssize_t rows = myBoard.getBoardRowCount();
        ssize_t cols = myBoard.getBoardColumnCount();
        
        //ASSERT
        REQUIRE(rows==3);
        REQUIRE(cols==4);
    }
}
