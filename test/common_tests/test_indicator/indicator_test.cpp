#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "../../../src/common/cell_indicator.hpp"

TEST_CASE("TEST INDICATOR") {
    SECTION("Test setting and getting cell state") {
        // ARRANGE
        CellIndicator indicator(CellState::COVERED, 3);

        // ACT
        indicator.setCellState(CellState::UNCOVERED);
        CellState currentState = indicator.getCellState();
        int bombCount = indicator.getBombCount();

        // ASSERT
        REQUIRE(currentState == CellState::UNCOVERED);
        REQUIRE(bombCount == 3);
    }

}
