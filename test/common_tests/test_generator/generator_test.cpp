#define CATCH_CONFIG_MAIN
#include"catch.hpp"

#include "../../../src/common/cell_board_generator.hpp"
#include "../../../src/common/cell.hpp"

TEST_CASE("TEST GENERATOR") {
    SECTION("Test CellBoardGenerator") {
        // ARRANGE
        CellBoardGenerator generator;

        // ACT
        CellBoard* board = generator.generateCellBoard(3, 3, 1); 

        // ASSERT
        REQUIRE(board != nullptr);

        delete board;
    }

}