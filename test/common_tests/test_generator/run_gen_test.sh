#!/bin/bash

g++ -std=c++17 ../../../src/common/cell_board_generator.cpp ../../../src/common/cell.cpp ../../../src/common/cell_board.cpp ../../../src/common/cell_bomb.cpp ../../../src/common/cell_indicator.cpp generator_test.cpp -o gen_test && ./gen_test
