#include <cstdlib>
#include <iostream>
#include <ostream>
#include "../src/netcode/packet_login_request.hpp"

void test_packet_login_request() {
  
  PacketLoginRequest* pack = new PacketLoginRequest("default_username", "0xDEADBEEF", "infdev");
  Packet* x = dynamic_cast<Packet*>(pack);
  PacketLoginRequest* backPack;
  backPack = dynamic_cast<PacketLoginRequest*>(x);
  
  if (backPack != nullptr) {
    std::cout << backPack << std::endl;
  }
}

void test_packet_login_request_serialization() {
  
  PacketLoginRequest* pack = new PacketLoginRequest("default_username", "0xCODEDEAD", "infdev");
  std::cout << pack << std::endl;
  std::cout << pack->toJson() << std::endl;

}

int main(void) { 
  test_packet_login_request();
  test_packet_login_request_serialization();

  exit(EXIT_SUCCESS);
}
