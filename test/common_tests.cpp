#include <cstdlib>
#include <ctime>
#include <iostream>
#include <memory>

#include "../src/common/cell_board.hpp"
#include "../src/common/cell_bomb.hpp"
#include "../src/common/cell_board_generator.hpp"

#define CCLR  "\033[0m"
#define CTEST "\u001b[33m"
#define CPASS "\u001b[32m"
#define CFAIL "\u001b[31m"

void test_bombs_only() {
  ssize_t rowCount = 5;
  ssize_t colCount = 5;

  std::cout << CTEST << "[T] INITIALIZING BOMBS ONLY TEST" << CCLR << std::endl;

  CellBoard* c1 = new CellBoard(rowCount, colCount);
  for (ssize_t row = 0; row < rowCount; row++) {
    for (ssize_t col = 0; col < colCount; col++) {
      c1->setCellToBomb(row, col, COVERED);
    }
  }

  delete c1;
  std::cout << CPASS << "[+] BOMBS ONLY TEST PASSED" << CCLR << std::endl;
}

void test_indicators_only() {
  ssize_t rowCount = 5;
  ssize_t colCount = 5;

  std::cout << CTEST << "[T] INITIALIZING INDICATOR ONLY TEST" << CCLR << std::endl;

  CellBoard* c1 = new CellBoard(rowCount, colCount);
  for (ssize_t row = 0; row < rowCount; row++) {
    for (ssize_t col = 0; col < colCount; col++) {
      c1->setCellToIndicator(row, col, UNCOVERED, 0);
    }
  }

  delete c1;
  std::cout << CPASS << "[+] INDICATOR ONLY TEST PASSED" << CCLR << std::endl;
}

void test_cell_board_generator(int rowCount, int colCount, int bombRatio) {

  std::cout << CTEST << "[T] INITIALIZING MAP CELL BOARD GENERATOR TEST" << CCLR << std::endl;
  CellBoardGenerator cb1 = CellBoardGenerator();
  CellBoard* c1 = cb1.generateCellBoard(rowCount, colCount, bombRatio);
  c1->printBoard();
  
  std::cout << CPASS << "[+] CELL BOARD GENERATOR TEST PASSED" << CCLR << std::endl;
}


void test_singular_random_bomb() {
  std::cout << CTEST << "[T] INITIALIZING RANDOM BOMB TEST" << CCLR << std::endl;
  srand((unsigned) time(NULL));
  ssize_t rowCount = 5;
  ssize_t colCount = 5;

  int randomRow = rand() % rowCount;
  int randomCol = rand() % colCount;

  int testScore = 0;
  int expectedScore = 1;

  CellBoard* c1 = new CellBoard(rowCount, colCount);
  c1->setCellToBomb(randomRow, randomCol, FLAGGED);

  for (ssize_t row = 0; row < rowCount; row++) {
    for (ssize_t col = 0; col < colCount; col++) {
      if (dynamic_cast<CellBomb*>(c1->getCellFromPosition(row, col)) != nullptr) {
        testScore++;
      }
    }
  }
  if (testScore != expectedScore) {
    std::cout << CFAIL << "[+] RANDOM BOMB TEST FAILED" << CCLR << std::endl;
    exit(EXIT_FAILURE);
  }

  delete c1;
  std::cout << CPASS << "[+] RANDOM BOMB TEST PASSED" << CCLR << std::endl;
}

int main(void) {
  test_bombs_only();
  test_indicators_only();
  test_singular_random_bomb();
  test_cell_board_generator(5, 5, 12);
  test_cell_board_generator(15, 15, 12);
  test_cell_board_generator(25, 25, 12);
  test_cell_board_generator(25, 50, 8);

  exit(EXIT_SUCCESS);
}

