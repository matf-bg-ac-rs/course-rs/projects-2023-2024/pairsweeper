# Naziv: Igranje igre

*Kratak opis:* Slučaj upotrebe koji nikada neće biti implementiran, već služi kao šablon za igranje partije u različitim režimima igre.

*Akteri:* Igrač.

*Preduslovi:* Aplikacija je pokrenuta i igrač je izabrao težinu i režim igre.

*Postuslovi:* Igra je završena a rezultat je potenciajlno upisan u listu najboljih. Aplikacija prikazuje glavni meni.

*Osnovni tok:*

1. U zavisnosti od težine, aplikacija pravi minsko polje i postavlja mine.
2. Kreira se tajmer koji krece od nule i koji će služiti za merenje rezultata.
3. Kreiraju se prozor za broj mina koji se nalazi na terenu.
4. U zavisnosti od režima igre prelazi se na jedan od sledećih slučajeva upotrebe: klasični, saradnja, 1v1. Igrač ili igrači započinju sa igrom.
5. Partija je završena.
5.1. U slučaju pobede preći na slučaj "sačuvaj rezultat".
5.2. U slučaju neuspeha otkriti sva polja.
6. Prikazati top 10 tabelu za režim odigrane partije i dugme za vraćanje na glavni meni.
7. Slučaj upotrebe se završava.

*Alternativni tokovi:* U slučaju neplaniranog izlaska iz igre, progres se odbacuje.

*Podtokovi:* /

*Specijalni uslovi:* Ažuriranja i sinhronizacija u realnom vremenu su neophodna za režime saradnje i 1v1.

*Dodatne informacije:*
1. Ovaj slučaj upotrebe predstavlja opšti opis svih režima igre (nezavisno od težine) i ne govori o pravilima i karakteristikama koji su vezani za pojedinačne režime.
2. Detalji funkcionisanja svakog od režima su navedeni u njihovim opisima.
