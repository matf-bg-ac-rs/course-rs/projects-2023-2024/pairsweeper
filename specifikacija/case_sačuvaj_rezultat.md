# Naziv: Sačuvaj rezultat

*Kratak opis:* Nakon uspešno završene partije, aplikacija čuva rezultat za određenu težinu i režim igre.

*Akteri:* Aplikacija automatski izvršava čuvanje.

*Preduslovi:* Partija je završena pobedom i ostvareni rezultat spada u 10 najboljih za tu težinu i režim igre.

*Postuslovi:* Ostvareni rezultat je uspešno sačuvan

*Osnovni tok:*
1. Aplikacija proverava da li ostvareni rezultat u datom režimu spada u 10 najboljih.
1.1. Ako spada, svaki igrač koji je učestvovao upisuje korisničko ime.
1.2. Ažurirati listu najboljih rezultata i postaviti ostvaren rezultat na odgovarajuće mesto.


*Alternativni tokovi:* Za slučaj neočekivanog izlaza iz igre za vreme partije, trenutno ostvareni rezultat se odbacuje i ništa se ne čuva.

*Podtokovi:* /

*Specijalni uslovi:* Konekcija sa internetom

*Dodatne informacije:* Tokom igranja igre postoji tajmer na osnovu kojeg će se pamtiti rezultati.
