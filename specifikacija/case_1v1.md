# Naziv: Režim igre 1v1

*Kratak opis:* Režim igre u kojem učestvuju dva igrača koji se takmiče jedan protiv drugog. Imaju zajedničko minsko polje i svako vidi direktne efekte sopstvenih akcija, kao i parcijalne efekte tudjih. Igra se završava za oba igrača u trenutku kada jedan od protivnika otvori polje sa minom, ili kada otvori sva ostala polja.

*Akteri:* Igrači.

*Preduslovi:* U podešavanjima je izabran režim igre "1v1" i ispunjeni su svi uslovi iz slučaja "igranje igre" (postavljene mine,
kreiran tajmer i prikazan prozor sa miniskim poljem).

*Postuslovi:* Igrač je kliknuo na polje sa minom ili nije ostalo više ni jedno polje bez mine.

*Osnovni tok:*
1. Svakom igraču je prikazan je prozor u kojem se nalazi minsko polje
2. Kreira se pratilac brojač poena (broj otkrivenih ćelija)
2. Za svakog igrača se pojedinačno odvija slučaj "jedan igrač"
3. Igrač koji prvi otkrije sve je pobedio
3.1 U slučaju da je jedan od igrača nagazio na minu, protivnik dobija poene i igra mu se završava.

*Alternativni tokovi:* Za slučaj neočekivanog izlaza iz igre za vreme partije, protivnik dobija poene.

*Podtokovi:* /

*Specijalni uslovi:*

*Dodatne informacije:* /

***
### Sekvencijalni dijagram: 1v1

```plantuml
@startuml

actor User

== Joining the matchmaking queue ==

User -> Application : Click "Join Matchmaking"
Application -> Server : Send MatchmakingJoinRequestPacket
Server -> Server : Add User to matchmaking queue
Server --> Application : Recv MatchmakingJoinResponsePacket
Application --> User : Display "Matchmaking" dialog


== Leaving the matchmaking queue ==

User -> Application : Click "Stop matchmaking" 
Application -> Server : Send LeaveMatchmakingQueueRequestPacket
Server -> Server: Remove User from matchmaking queue
Server --> Application : Send LeaveMatchmakingResponsePacket
Application --> User: Undisplay "Matchmaking" dialog

== Opponent found ==

Server -> Application: Send MultiplayerGameStartRequestPacket
Application -> User: Display MultiplayerGameView
User -> Application: Click on a cell
Application -> Server: Send CellActionRequestPacket 
Server -> Server: Update current user gamestate
Server --> Application: Send CellActionResponsePacket
Application --> User: Show updated cell board

== Game over ==

Server --> Application : Send GameOverPacket
User <-- Application : Return user to matchmaking screen
Server -> Server : cleanup


@enduml
```
