# Naziv: Podešavanja

*Kratak opis:* Pre početka partije biramo u kakvom režimu igre ćemo igrati i na kojoj težini. Ovo je prvo na šta nailazimo pri pokretanju aplikacije.

*Akteri:* Igrač.

*Preduslovi:* Pokrenuli smo aplikaciju.

*Postuslovi:* Aplikacija je inicijalizovana vrednosti potrebne za igranje igre.

*Osnovni tok:*
1. Igrač je pokrenuo aplikaciju i prikazuje mu se prozor sa opcijama za biranje težine.
2. Igrač sada bira režim igre u kojem će se igrati.
2.1. U slučaju da je odabran režim igre "jedan igrač" prelazi se na slučaj "igranje igre".
2.2. U suprotnom prelazi se na slučaj "uspostavi konekciju" i nakon toga na slučaj "igranje igre".

*Alternativni tokovi:* Za slučaj neočekivanog izlaza iz igre za vreme partije, sve što smo podesili se odbacuje.

*Podtokovi:* /

*Specijalni uslovi:* /

*Dodatne informacije:* /
