# Naziv: Prikaži najbolje

*Kratak opis:* Ova opcija omogućava igračima da vide 10 najboljih prethodno ostvarenih rezultata u zavisnosti od težine i režima igre.

*Akteri:* Igrač

*Preduslovi:* Igra je pokrenuta i nalazimo se u glavnom meniju

*Postuslovi:* /

*Osnovni tok:*
1. Igrač je iz glavnog menija kliknuo na dugme za tabelu najboljih rezultata.
2. Aplikacija prikazuje prozor za izbor za režima igre.
3. Igrač bira režim i nakon toga mu se pojavljuje prozor sa listama 10 najboljih rezultata za svaku od tri težine.
4. Igrač pritiskom na dugme za izlazak iskljucuje tabelu i vraća se u glavni meni. Slučaj upotrebe se završava.


*Alternativni tokovi:* /

*Podtokovi:* /

*Specijalni uslovi:* /

*Dodatne informacije:* Za režim igre sa jednim igračem, rezultat je oblika: "<igrač> <vreme>", za režime sa više igrača, rezultat je u formatu "<igrač1> <igrač2> <vreme>".
