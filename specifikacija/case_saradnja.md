# Naziv: Režim igre saradnja

*Kratak opis:* Režim igre u kojem ušestvuje više igrača koji zajedno pokušavaju da otkriju sve čiste ćelije.

*Akteri:* Igrači.

*Preduslovi:* U podešavanjima je izabran režim igre "saradnja" i ispunjeni su svi uslovi iz slučaja "igranje igre" (postavljene mine, kreiran tajmer i prikazan prozor sa miniskim poljem).

*Postuslovi:* Igrač je kliknuo na polje sa minom ili nije ostalo više ni jedno polje bez mine.

*Osnovni tok:*
1. Svakom igraču je prikazan je prozor u kojem se nalazi minsko polje.
2. Svaki igrač za vreme svog poteza ima pravo da obeleži ili otkrije jedno polje.
3. U slučaju da jedan igrač stane na minu, svi igrači gube.

*Alternativni tokovi:* Za slučaj neočekivanog izlaza iz igre za vreme partije, sav ostvaren rezultat se odbacuje.

*Podtokovi:* /

*Specijalni uslovi:* /

*Dodatne informacije:* /
