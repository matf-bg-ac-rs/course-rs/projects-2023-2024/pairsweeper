# Naziv: Jedan igrač

*Kratak opis:* Igra je otpočela u nekoj težini u režimu koji podrazumeva jednog igrača. Igrač klikovima mišem otkriva ćelije polja dok ne otkrije sve ćelije na kojima nisu mine ili dok ne otkrije onu na kojoj se nalazila mina u kada dolazi do kraja partije i smatra se da je igrač izgubio.

*Akteri:* Igrač.

*Preduslovi:* Iz slučaja "Igranje igre" smo prešli u ovaj što znači igrač je upisao ime, minsko polje je postavljeno i kreiran je tajmer. Igrač pred sobom ima minsko polje i ostvareni progres.

*Postuslovi:* Partija je završena.

*Osnovni tok:*
1. Prikazan je prozor u kojem se nalazi minsko polje.
2. Ranije kreiran tajmer počinje sa merenjem vremena
3. Igrač koristi miš za interakciju sa ćelijama.
3.1 Levi klik otkriva ćeliju.
3.2 Desni klik menja oznaku ćelije znakovima 'zastava' i bez znaka.
4. Ako je igrač obeležio ćeliju zastavom, broj mina u prozoru se umanjuje za 1.
5. Igrač je otkrio ćeliju.
5.1. Na čeliji nije bilo mine. Igrač je pobedio i prelazi se na korak 5 iz toka za use case 'igranje igre'.
5.2. Na ćeliji je bila mina. Igrač je izgubio partiju i prelazi se na korak 5 iz toka za use case 'igranje igre'.

*Alternativni tokovi:*
1. Igrač ima mogućnost da prekine igru i da se vrati na glavni meni.
2. Igrač može da pauzira igru i tada se tajmer zaustavlja, a minsko polje 'sakriva'.

*Podtokovi:* /

*Specijalni uslovi:* /

*Dodatne informacije:* /
