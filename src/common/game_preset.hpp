#ifndef GAME_PRESET_HPP
#define GAME_PRESET_HPP

enum difficulty{
    EASY,
    NORMAL,
    HARD
};

enum gameMode{
    SINGLEPLAYER,
    COOP,
    PVP
};

class GamePreset {
private:
    int rows;
    int cols;
    int numMines;
    gameMode mode;
    difficulty dif;

public:
    GamePreset(int r, int c, int mines, gameMode gMode, difficulty d);
    
    int getRows() const;
    int getCols() const;
    int getNumMines() const;
    int getGameMode() const;
};
#endif

