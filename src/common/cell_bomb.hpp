#ifndef CELL_BOMB_HPP
#define CELL_BOMB_HPP

#include "cell.hpp"

class CellBomb : public Cell {
public:
  CellBomb(CellState cellstate);

  CellState getCellState() const override;

  void setCellState(CellState cellState) override;
};

#endif

