#include "cell_indicator.hpp"
#include "cell.hpp"

CellIndicator::CellIndicator(CellState cellState, int bombCount) : Cell(cellState), _bombCount(bombCount) {}

CellState CellIndicator::getCellState() const {
  return _cellState;
}

void CellIndicator::setCellState(CellState cellState) {
  _cellState = cellState;
}

int CellIndicator::getBombCount() const {
  return _bombCount;
}

