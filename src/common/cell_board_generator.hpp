#ifndef CELL_BOARD_GENERATOR_HPP
#define CELL_BOARD_GENERATOR_HPP

#include "cell_board.hpp"

class CellBoardGenerator {
public:
  CellBoardGenerator();
  CellBoard* generateCellBoard(ssize_t boardRowCount, ssize_t boardColumnCount, int bombRatio);
  CellBoard* generateEmptyCellBoard(ssize_t boardRowCount, ssize_t boardColumnCount);

};

#endif

