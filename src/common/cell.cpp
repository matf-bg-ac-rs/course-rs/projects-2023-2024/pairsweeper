#include "cell.hpp"

Cell::~Cell() {};

Cell::Cell(CellState cellState) 
: _cellState(cellState) {
};

CellState Cell::getCellState() const {
  return _cellState;
}

void Cell::setCellState(CellState cellState) {
  _cellState = cellState;
}

