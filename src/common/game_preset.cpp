#include "game_preset.hpp"

GamePreset::GamePreset(int r, int c, int mines, gameMode gMode,difficulty d) 
    : rows(r), cols(c), numMines(mines), mode(gMode), dif(d){
}

int GamePreset::getRows() const {
    return rows;
}

int GamePreset::getCols() const {
    return cols;
}

int GamePreset::getNumMines() const {
    return numMines;
}

int GamePreset::getGameMode() const {
    return mode;
}

