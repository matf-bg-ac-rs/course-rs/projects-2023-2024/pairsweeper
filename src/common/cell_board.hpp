#ifndef CELL_BOARD_HPP
#define CELL_BOARD_HPP

#include "cell.hpp"

#include <sys/types.h>
#include <vector>
#include <string>

class CellBoard {
private:
  const ssize_t _boardRowCount;
  const ssize_t _boardColumnCount;

  std::vector<std::vector<Cell*>> _cellVector;

public:
  ~CellBoard();
  CellBoard(ssize_t boardRowCount, ssize_t boardColumnCount);
  CellBoard(ssize_t boardRowCount, ssize_t boardColumnCount, std::string cellstring);

  ssize_t getBoardRowCount(){
    return _boardRowCount;
  }
  ssize_t getBoardColumnCount(){
    return _boardColumnCount;
  }

  int getNearbyBombCountFromPosition(int rowPosition, int columnPosition);

  Cell* getCellFromPosition(ssize_t rowPosition, ssize_t columnPosition);
  
  std::string CellBoardToString();


  void setCellToBomb(ssize_t rowPosition, ssize_t columnPosition, CellState cellState);
  void setCellToIndicator(ssize_t rowPosition, ssize_t columnPosition, CellState cellState, int bombCount);
  void printBoard();
};

#endif

