#include "cell_bomb.hpp"

CellBomb::CellBomb(CellState cellState) : Cell(cellState) {}

CellState CellBomb::getCellState() const {
  return _cellState;
}

void CellBomb::setCellState(CellState cellState) {
  _cellState = cellState;
}

