#ifndef CELL_HPP
#define CELL_HPP

enum CellState {
  COVERED,
  UNCOVERED,
  FLAGGED,
};

class Cell {
protected:
  CellState _cellState;

public:
  virtual ~Cell();
  Cell(CellState cellState);

  virtual CellState getCellState() const = 0;

  virtual void setCellState(CellState cellState) = 0;
};

#endif

