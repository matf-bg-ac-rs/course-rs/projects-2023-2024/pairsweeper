#include "cell_board_generator.hpp"
#include "cell.hpp"
#include "cell_board.hpp"
#include "cell_bomb.hpp"
#include <cstdlib>
#include <unordered_map>
#include <utility>

struct pair_hash {
  template <class T1, class T2>
  std::size_t operator () (const std::pair<T1,T2> &p) const {
    auto h1 = std::hash<T1>{}(p.first);
    auto h2 = std::hash<T2>{}(p.second); 

    return h1 ^ h2; 
  }
};

CellBoardGenerator::CellBoardGenerator(){};
CellBoard* CellBoardGenerator::generateCellBoard(ssize_t boardRowCount, ssize_t boardColumnCount, int bombRatio) {
  // 1) place bombs randomly
  // 2) create indicators around the bombs

  CellBoard* c1 = new CellBoard(boardRowCount, boardColumnCount);
  int numberOfBombs = (boardRowCount * boardColumnCount) / bombRatio;

  std::unordered_map<std::pair<ssize_t, ssize_t>, int, pair_hash> bombMap;
  for (int i = 0; i < numberOfBombs; i++) {
    int randomRow, randomColumn;
    randomRow = random() % boardRowCount;
    randomColumn = random() % boardColumnCount;
    std::pair posPair = std::make_pair(randomRow, randomColumn);

    auto it = bombMap.find(posPair);

    if (it != bombMap.end()) {
      i--;
      continue;
    } else {
      bombMap[posPair] = 0xBBBB;
    }
  }

  for (auto it = bombMap.begin(); it != bombMap.end(); ++it) {
    c1->setCellToBomb(it->first.first, it->first.second, COVERED);
  }

  for (int i = 0; i < boardRowCount; i++) {
    for (int j = 0; j < boardColumnCount; j++) {
      if (dynamic_cast<CellBomb*>(c1->getCellFromPosition(i,j)) == nullptr) {
        int bombCount = 0;
        bombCount = c1->getNearbyBombCountFromPosition(i, j);
        c1->setCellToIndicator(i, j, COVERED, bombCount);

      }
    }
  }

  return c1;
}

CellBoard* CellBoardGenerator::generateEmptyCellBoard(ssize_t boardRowCount, ssize_t boardColumnCount) {

  CellBoard* c1 = new CellBoard(boardRowCount, boardColumnCount);
  std::unordered_map<std::pair<ssize_t, ssize_t>, int, pair_hash> bombMap;

  for (int i = 0; i < boardRowCount; i++) {
    for (int j = 0; j < boardColumnCount; j++) {
        c1->setCellToIndicator(i, j, CellState::COVERED, 0);
    }
  }

  return c1;
}
