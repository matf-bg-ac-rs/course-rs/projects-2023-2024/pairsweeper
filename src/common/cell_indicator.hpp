#ifndef CELL_INDICATOR_HPP
#define CELL_INDICATOR_HPP

#include "cell.hpp"

class CellIndicator : public Cell {
private:
  int _bombCount;
public:
  CellIndicator(CellState cellstate, int bombCount);

  CellState getCellState() const override;

  void setCellState(CellState cellState) override;

  int getBombCount() const;
};

#endif

