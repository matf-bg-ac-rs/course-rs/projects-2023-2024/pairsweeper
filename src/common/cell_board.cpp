#include "cell_board.hpp"
#include "cell.hpp"
#include "cell_bomb.hpp"
#include "cell_indicator.hpp"

#include <iostream>
#include <memory>
#include <sys/types.h>
#include <vector>
#include <string>

#define BCLR  "\033[0m"
#define BBOMB "\u001b[31m"

CellBoard::CellBoard(ssize_t boardRowCount, ssize_t boardColumnCount)
  : 
  _boardRowCount(boardRowCount),
  _boardColumnCount(boardColumnCount),
  _cellVector(boardRowCount, std::vector<Cell*> (boardColumnCount, nullptr))
{};

CellBoard::CellBoard(ssize_t boardRowCount, ssize_t boardColumnCount, std::string cellstring)
  : 
  _boardRowCount(boardRowCount),
  _boardColumnCount(boardColumnCount),
  _cellVector(boardRowCount, std::vector<Cell*> (boardColumnCount, nullptr))
{

  for (int i = 0; i < _boardRowCount; i++) {
    for (int j = 0; j < _boardColumnCount; j++) {
      int cellPos = i*14 + j;

      if (cellstring.at(cellPos) == 'b') {
        setCellToBomb(i, j, CellState::UNCOVERED);
      }
      if (cellstring.at(cellPos) == 'c') {
        setCellToIndicator(i, j, CellState::COVERED, 0);
      }
      if (cellstring.at(cellPos) == 'f') {
        setCellToIndicator(i, j, CellState::FLAGGED, 0);
      }
      for (int i = 0; i < 9; i++) {
        if (cellstring.at(cellPos) == ('0' + i)) {
          setCellToIndicator(i, j, CellState::UNCOVERED, i);
        }
      }
    }
  }
};

CellBoard::~CellBoard() {
  for (auto row : _cellVector) {
    for (auto col : row) {
      delete col;
    }
  }
}

Cell* CellBoard::getCellFromPosition(ssize_t rowPosition, ssize_t columnPosition) {
  return _cellVector[rowPosition][columnPosition];
}

// TODO: generate test in case of 9, to regenerate the map
int CellBoard::getNearbyBombCountFromPosition(int rowPosition, int columnPosition) {
  int bombCount = 0;
  for (int i = -1; i <= 1; i++) {
    for (int j = -1; j <= 1; j++) {
      if (rowPosition + i < 0 || columnPosition + j < 0 || rowPosition + i >= _boardRowCount || columnPosition +j >= _boardColumnCount) continue;
      Cell* c = getCellFromPosition(rowPosition + i, columnPosition + j);
      if (dynamic_cast<CellBomb*>(c) != nullptr) {
        bombCount++;
      }
    }
  }
  return bombCount;
}

void CellBoard::setCellToBomb(ssize_t rowPosition, ssize_t columnPosition, CellState cellState) {
  _cellVector[rowPosition][columnPosition] = new CellBomb(cellState);
}
void CellBoard::setCellToIndicator(ssize_t rowPosition, ssize_t columnPosition, CellState cellState, int bombCount) {
  _cellVector[rowPosition][columnPosition] = new CellIndicator(cellState, bombCount);
}

void CellBoard::printBoard() {
  for (int i = 0; i < _boardRowCount; i++) {
    for (int j = 0; j < _boardColumnCount; j++) {
      Cell* c = getCellFromPosition(i, j);
      if (dynamic_cast<CellBomb*>(c) != nullptr) {
        std::cout << BBOMB << "*" << BCLR;

      } else {
        CellIndicator* ci = dynamic_cast<CellIndicator*>(c);
        std::cout << ci->getBombCount();
      }
    }
    std::cout << std::endl;
  }

}

std::string CellBoard::CellBoardToString() {
  std::string strCellBoard = "";

  for (int i = 0; i < _boardRowCount; i++) {
    for (int j = 0; j < _boardColumnCount; j++) {

      Cell* c = getCellFromPosition(i, j);

      CellIndicator* ci = dynamic_cast<CellIndicator*>(c);
      if (ci != nullptr) {
        if (ci->getCellState() == CellState::COVERED) {
          strCellBoard += 'c';
        }
        else if (ci->getCellState() == CellState::UNCOVERED) {
          strCellBoard += '0' + ci->getBombCount();
        }
        else if (ci->getCellState() == CellState::FLAGGED) {
          strCellBoard += 'f';
        }
      }
      else if (dynamic_cast<CellBomb*>(c) != nullptr) {
        CellBomb* cellBomb = dynamic_cast<CellBomb*>(c);
        if (cellBomb->getCellState() == CellState::COVERED) {
          strCellBoard += 'c';
        } else if (cellBomb->getCellState() == CellState::FLAGGED) {
          strCellBoard += 'f';
        } else {
          strCellBoard += 'b';
        }
      }
  }
}
  return strCellBoard;
}