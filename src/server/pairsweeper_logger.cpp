#include "pairsweeper_logger.hpp"

void Logger::info(const std::string& message) {
  log("INFO", message);
}

void Logger::error(const std::string& message) {
  log("ERROR", message);
}

void Logger::log(const std::string& level, const std::string& message) {
  std::ofstream file("log.txt", std::ios_base::app); // Append mode
  if (!file) {
      std::cerr << "Failed to open log file\n";
      return;
  }

  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);

  file << '[' << std::put_time(&tm, "%Y-%m-%d %H:%M:%S") << "] ";
  file << level << ": " << message << '\n';
  file.close();
}
