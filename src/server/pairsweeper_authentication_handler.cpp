#include "pairsweeper_authentication_handler.hpp"
#include <mutex>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <sstream>
#include <string>

PairsweeperAuthenticationHandler* PairsweeperAuthenticationHandler::instance = nullptr;

PairsweeperAuthenticationHandler::PairsweeperAuthenticationHandler() {
	_filename = ".auth.txt";
  _userMap = std::map<std::string, std::string>();
	loadData();
}

void PairsweeperAuthenticationHandler::loadData() { 
  std::ofstream file(_filename, std::ios::app);

  if (!file) {
    std::cerr << "[ERROR] create file" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::ifstream authFile;
  authFile.open(_filename);

  if (authFile.is_open()) {

    std::string line;
    while (std::getline(authFile, line)) {
      std::string username;
      std::string passwordHash;

      char delimiter = ':';
      size_t delimiterPos = line.find(delimiter);

      if (delimiterPos != std::string::npos) {
        username = line.substr(0, delimiterPos);
        passwordHash = line.substr(delimiterPos+1);
        _userMap.insert(std::make_pair(username, passwordHash));
      } else {
        std::cerr << "[ERROR] Delimiter not found" << std::endl;
      }
    }

    authFile.close();    
  } else {
    std::cerr << "[ERROR] open file" << std::endl;
    exit(EXIT_FAILURE);
  }

}

void PairsweeperAuthenticationHandler::saveData(std::string username, std::string passwordHash) {
   std::ofstream authFile;
   authFile.open(_filename, std::ios::app);

   if (!authFile) {
       std::cerr << "[ERROR] Failed to open file for writing" << std::endl;
       exit(EXIT_FAILURE);
   }

    authFile << username << ':' << passwordHash << '\n';

   authFile.close();
}

PairsweeperAuthenticationHandler* PairsweeperAuthenticationHandler::getInstance() {
	if (instance == nullptr) {
		instance = new PairsweeperAuthenticationHandler();
	}
	return instance;
}

bool PairsweeperAuthenticationHandler::registerUser(std::string username, std::string passwordHash) {
  _userMap.insert(std::make_pair(username, passwordHash));
  _authFileMutex.lock();
  saveData(username, passwordHash);
  _authFileMutex.unlock();
  return true;
}

bool PairsweeperAuthenticationHandler::validateUser(std::string username, std::string passwordHash) {

 if (_userMap.find(username) == _userMap.end()) {
  registerUser(username, passwordHash);
  std::cout << "[REGISTER] New user registered" << std::endl;
  return true;
 }
 
 if (_userMap.at(username) != passwordHash) {
  std::cout << "[LOGIN] Invalid login credentials" << std::endl;
      return false;
 }

  std::cout << "[LOGIN] User logged in." << std::endl;
 return true;
}
bool encryptString(const std::string& plaintext, std::string& ciphertext) {
   std::string key = "secretKey";
   ciphertext = "";

   for (std::string::size_type i = 0; i < plaintext.size(); ++i)
       ciphertext += plaintext[i] ^ key[i % key.size()];

   return true;
}

bool decryptString(const std::string& ciphertext, std::string& plaintext) {
   std::string key = "secretKey";
   plaintext = "";

   for (std::string::size_type i = 0; i < ciphertext.size(); ++i)
       plaintext += ciphertext[i] ^ key[i % key.size()];

   return true;
}