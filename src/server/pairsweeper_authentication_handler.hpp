#ifndef PAIRSWEEPER_AUTHENTICATION_HANDLER_HPP
#define PAIRSWEEPER_AUTHENTICATION_HANDLER_HPP

#include <map>
#include <string>
#include <fstream>
#include <mutex>

class PairsweeperAuthenticationHandler {
	private:
		std::mutex _authFileMutex;
		std::map<std::string, std::string> _userMap;
		std::string _filename;
		static PairsweeperAuthenticationHandler* instance;

		PairsweeperAuthenticationHandler();

		void loadData();
		void saveData(std::string username, std::string passwordhash);

		bool encryptString(const std::string& plaintext, std::string& ciphertext);
		bool decryptString(const std::string& ciphertext, std::string& plaintext);

	public:
		PairsweeperAuthenticationHandler(PairsweeperAuthenticationHandler const&) = delete;
		void operator=(PairsweeperAuthenticationHandler const&) = delete;

		static PairsweeperAuthenticationHandler* getInstance();

		bool registerUser(std::string username, std::string passwordHash);
		bool validateUser(std::string username, std::string passwordHash);
};

#endif
