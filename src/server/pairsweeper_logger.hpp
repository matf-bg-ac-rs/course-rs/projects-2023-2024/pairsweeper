#ifndef PAIRSWEEPER_LOGGER_HPP
#define PAIRSWEEPER_LOGGER_HPP

#include <string>
#include <fstream>
#include <ctime>
#include <iostream>
#include <iomanip>

class Logger {
public:
  static void info(const std::string& message);
  static void error(const std::string& message);

private:
  static void log(const std::string& level, const std::string& message);
};

#endif
