// std
#include <iostream>
#include <string>
#include <unordered_map>
#include <random>
#include <thread>
#include <algorithm>

// poco
#include <Poco/Net/Socket.h>
#include <Poco/Net/StreamSocket.h>
#include <Poco/Net/TCPServer.h>
#include <Poco/Net/TCPServerConnection.h>
#include <Poco/Net/TCPServerConnectionFactory.h>
#include <Poco/Net/TCPServerParams.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/HelpFormatter.h>

// pairsweeper
#include "pairsweeper_authentication_handler.hpp"
#include "../netcode/packet_login_request.hpp"
#include "../netcode/packet_login_response.hpp"
#include "../netcode/packet_matchmaking_join_response.hpp"
#include "../netcode/packet_matchmaking_leave_response.hpp"
#include "../netcode/packet_matchmaking_join_request.hpp"
#include "../netcode/packet_matchmaking_leave_request.hpp"
#include "../netcode/packet_match_start_response.hpp"
#include "../netcode/packet_parser.hpp"
#include "../netcode/packet_cell_open_request.hpp"
#include "../common/cell_board.hpp"
#include "../common/cell_board_generator.hpp"
#include "../common/cell_bomb.hpp"
#include "../common/cell_indicator.hpp"
#include "../netcode/packet_match_state_response.hpp"
#include "../netcode/packet_match_end_response.hpp"

const std::string VALID_CHARS = "abcdefghijklmnopqrstuvwxyz";

std::string generate_identifier()
{
  std::random_device rd;
  std::default_random_engine generator(rd());
  std::uniform_int_distribution<int> distribution(0, VALID_CHARS.size() - 1);
  std::string random_string;
  std::generate_n(std::back_inserter(random_string), 16, [&]()
                  { return VALID_CHARS[distribution(generator)]; });
  return random_string;
}

class PlayerConnection
{
public:
  std::string get_username()
  {
    return _username;
  }

  Poco::Net::StreamSocket *get_socket()
  {
    return _sock;
  }

  PlayerConnection(std::string username, Poco::Net::StreamSocket *sock)
  {
    _username = username;
    _sock = sock;
  };

private:
  std::string _username;
  Poco::Net::StreamSocket *_sock;
};

class Match
{

private:
  bool _running;
  CellBoard *_cellBoard;
  int _player1Points;
  int _player2Points;
  PlayerConnection *_player1Connection;
  PlayerConnection *_player2Connection;

public:
  bool isRunning() {return _running;};
  Match(PlayerConnection *player1Connection, PlayerConnection *player2Connection) : _running(true),
                                                                                    _cellBoard(CellBoardGenerator().generateCellBoard(14, 14, 12)),
                                                                                    _player1Points(0),
                                                                                    _player2Points(0),
                                                                                    _player1Connection(player1Connection),
                                                                                    _player2Connection(player2Connection)
  {
    std::cout << _cellBoard->CellBoardToString() << std::endl;
  };

  void notifyMatchStart()
  {
    // Paket za prvog igraca
    PacketMatchStartResponse *packetMatchStart1 =
        new PacketMatchStartResponse(_player2Connection->get_username());
    std::string jsonstring1 = packetMatchStart1->toJson();

    // Paket za drugog igraca
    PacketMatchStartResponse *packetMatchStart2 =
        new PacketMatchStartResponse(_player1Connection->get_username());
    std::string jsonstring2 = packetMatchStart2->toJson();

    std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // TODO: timer obrada

    _player1Connection->get_socket()->sendBytes(jsonstring1.c_str(), jsonstring1.size());

    _player2Connection->get_socket()->sendBytes(jsonstring2.c_str(), jsonstring2.size());
    delete packetMatchStart1;
    delete packetMatchStart2;
  }

  void handlePlayerDisconnect(PlayerConnection *disconnectedPlayer)
  {
    _running = false;
    std::string winner;
    if (_player1Connection == disconnectedPlayer)
    {
      winner = _player2Connection->get_username();
    }
    else
    {
      winner = _player1Connection->get_username();
    }
    PacketMatchEndResponse *packetMatchEnd =
        new PacketMatchEndResponse(winner, _player1Points, _player2Points);
    std::string jsonstring = packetMatchEnd->toJson();

    if (_player1Connection == disconnectedPlayer)
    {
      _player2Connection->get_socket()->sendBytes(jsonstring.c_str(), jsonstring.size());
    }
    else
    {
      _player1Connection->get_socket()->sendBytes(jsonstring.c_str(), jsonstring.size());
    }
    delete packetMatchEnd;
  }

  void playMove(PlayerConnection *pconn, int row, int column)
  {


    Cell *genericCell = _cellBoard->getCellFromPosition(row, column);

    if (dynamic_cast<CellIndicator *>(genericCell) != nullptr)
    {
      if ((dynamic_cast<CellIndicator *>(genericCell))->getCellState() == CellState::UNCOVERED)
      {
        return;
      }
      if (_player1Connection == pconn)
      {
        _player1Points++;
      }
      if (_player2Connection == pconn)
      {
        _player2Points++;
      }
      genericCell->setCellState(CellState::UNCOVERED);
    }

    if (dynamic_cast<CellBomb *>(genericCell) != nullptr)
    {
      std::string winner;
      if (_player1Connection == pconn)
      {
        winner = _player2Connection->get_username();
      }
      else
      {
        winner = _player1Connection->get_username();
      }

      PacketMatchEndResponse *packetMatchEnd =
          new PacketMatchEndResponse(winner, _player1Points, _player2Points);

      std::string jsonstring = packetMatchEnd->toJson();
      _player1Connection->get_socket()->sendBytes(jsonstring.c_str(), jsonstring.size());
      _player2Connection->get_socket()->sendBytes(jsonstring.c_str(), jsonstring.size());
      delete packetMatchEnd;
    }
    else
    {
      PacketMatchStateResponse *packetMatchState1;
      PacketMatchStateResponse *packetMatchState2;

      packetMatchState1 = new PacketMatchStateResponse(_cellBoard->CellBoardToString(), _player2Points, _player1Points);
      packetMatchState2 = new PacketMatchStateResponse(_cellBoard->CellBoardToString(), _player1Points, _player2Points);

      std::string jsonstring1 = packetMatchState1->toJson();
      std::string jsonstring2 = packetMatchState2->toJson();

      _player1Connection->get_socket()->sendBytes(jsonstring1.c_str(), jsonstring1.size());
      _player2Connection->get_socket()->sendBytes(jsonstring2.c_str(), jsonstring2.size());
      delete packetMatchState1;
      delete packetMatchState2;
    }
  }
};

class ActiveMatches
{
private:
  std::unordered_map<PlayerConnection *, Match *> _matches;
  // Private constructor so that no objects can be created.
  ActiveMatches() {}

  // Copy constructor is private
  ActiveMatches(const ActiveMatches &obj) = delete;

  // Instance pointer
  static ActiveMatches *instancePtr;

public:
  // Destructor
  ~ActiveMatches() {}

  // Static method to get instance
  static ActiveMatches *getInstance()
  {
    if (instancePtr == nullptr)
    {
      instancePtr = new ActiveMatches();
    }
    return instancePtr;
  }

  void addMatch(PlayerConnection *player1Connection, PlayerConnection *player2Connection, Match *match)
  {
    _matches.insert(std::make_pair(player1Connection, match));
    _matches.insert(std::make_pair(player2Connection, match));
    std::cout << "match added" << std::endl;
  }

  Match *getMatchFromConnection(PlayerConnection *pc)
  {
    if (_matches.find(pc) != _matches.end())
    {
      Match* activeMatch = _matches.at(pc);
        return activeMatch;
    }

    return nullptr;
  }

  void removeMatch(PlayerConnection *player1Connection, PlayerConnection *player2Connection)
  {
    _matches.erase(player1Connection);
    _matches.erase(player2Connection);
  }
};

ActiveMatches *ActiveMatches::instancePtr = nullptr;

class LobbyState
{
public:
  void print_connected_stats()
  {
    for (std::unordered_map<std::string, PlayerConnection *>::iterator
             key_it = _connectedPlayers.begin();
         key_it != _connectedPlayers.end(); key_it++)
    {
      std::cout << key_it->first << "[" << key_it->second->get_username() << "]" << std::endl;
    }
  }

  void add_connection(std::string playerID, PlayerConnection *playerConn)
  {
    auto it = _connectedPlayers.find(playerID);
    if (it != _connectedPlayers.end())
    {
      std::cout << "already logged in " << std::endl;
      return;
    }
    std::pair<std::string, PlayerConnection *> insertPair(playerID, playerConn);

    _connectedPlayers.insert(insertPair);
    _connectionCounter++;
  }
  void remove_connection(std::string playerID)
  {
    auto it = _connectedPlayers.find(playerID);
    if (it == _connectedPlayers.end())
    {
      return;
    }

    PlayerConnection *connToRemove = _connectedPlayers[playerID];

    // Provera da li je igrac u aktivnoj partiji
    Match *activeMatch = ActiveMatches::getInstance()->getMatchFromConnection(connToRemove);
    if (activeMatch != nullptr)
    {
      activeMatch->playMove(connToRemove, -1, -1);
    }

    _connectedPlayers.erase(playerID);
    _connectionCounter--;
    delete connToRemove;
  }

  void add_to_matchmaking_queue(PlayerConnection *playerConnection)
  {
    _queuedPlayers.push_back(playerConnection);

    if (_queuedPlayers.size() >= 2)
    {
      PlayerConnection *conn1 = _queuedPlayers.back();
      _queuedPlayers.pop_back();
      PlayerConnection *conn2 = _queuedPlayers.back();
      _queuedPlayers.pop_back();

      start_match(conn1, conn2);
    }
  }

  void remove_from_matchmaking_queue(PlayerConnection *playerConnection)
  {
    _queuedPlayers.pop_back();
  }

  void start_match(PlayerConnection *connA, PlayerConnection *connB)
  {
    ActiveMatches *activeMatches = ActiveMatches::getInstance();
    std::cout << "starting match between two parties" << std::endl;
    Match *match = new Match(connA, connB);
    activeMatches->addMatch(connA, connB, match);
    match->notifyMatchStart();
  }

  int get_connected_count()
  {
    return _connectedPlayers.size();
  }

  static LobbyState &getInstance()
  {
    static LobbyState instance;
    return instance;
  }

  LobbyState(const LobbyState &) = delete;
  LobbyState &operator=(const LobbyState &) = delete;

private:
  std::unordered_map<std::string, PlayerConnection *> _connectedPlayers;
  std::vector<PlayerConnection *> _queuedPlayers;

  int _connectionCounter;
  LobbyState()
  {
    std::cout << "[+] Initialized lobby state" << std::endl;
    _connectionCounter = 0;
    _connectedPlayers = std::unordered_map<std::string, PlayerConnection *>();
    _queuedPlayers = std::vector<PlayerConnection *>();
  }
};

class EchoConnection : public Poco::Net::TCPServerConnection
{
public:
  EchoConnection(const Poco::Net::StreamSocket &socket) : Poco::Net::TCPServerConnection(socket)
  {
  }

  void run()
  {
    std::string playerIdentifier = generate_identifier();
    PlayerConnection *pc = nullptr;
    bool authenticated = false;
    LobbyState &lobbyState = LobbyState::getInstance();
    Poco::Buffer<char> buffer(1024);
    int n = socket().receiveBytes(buffer.begin(), buffer.size());
    while (n > 0)
    {
      if (n <= 0)
      {
        std::cout << "[DISCONNECT]" << std::endl;
        lobbyState.remove_connection(playerIdentifier);
        return;
      }

      std::string receivedData(buffer.begin(), buffer.begin() + n);
      std::cout << receivedData << std::endl;

      PacketRequest *pi =
          PacketParser::parse_inbound_packet(receivedData);

      // LOGIN PACKET
      if (dynamic_cast<PacketLoginRequest *>(pi) != nullptr)
      {
        PacketLoginRequest *packLoginRequest = PacketLoginRequest::fromJson(receivedData);
        std::cout << packLoginRequest->get_username() << " is trying to login" << std::endl;

        authenticated = PairsweeperAuthenticationHandler::getInstance()->validateUser(packLoginRequest->get_username(), packLoginRequest->getHash());
        std::cout << "[AUTH] status : " << authenticated << std::endl;

        if (authenticated == false)
        {
          delete packLoginRequest;
          return;
        }
        // ADD PLAYER TO CONNECTED LIST
        pc =
            new PlayerConnection(
                packLoginRequest->get_username(),
                &socket());
        lobbyState.add_connection(
            playerIdentifier,
            pc);


        delete packLoginRequest;
        
        PacketLoginResponse* pack = new PacketLoginResponse();
        std::string jsonstring = pack->toJson();
        socket().sendBytes(jsonstring.c_str(), jsonstring.size());
        
        delete pack;
      }
      // LOGIN PACKET
      // MATCHMAKING QUEUE JOIN PACKET
      else if (dynamic_cast<PacketMatchmakingJoinRequest *>(pi) != nullptr)
      {
        std::cout << "Someone has joined the matchmaking queue" << std::endl;
        PacketMatchmakingJoinResponse *respPack = new PacketMatchmakingJoinResponse();
        std::string jsonstring = respPack->toJson();
        socket().sendBytes(jsonstring.c_str(), jsonstring.size());
        LobbyState::getInstance().add_to_matchmaking_queue(pc);
        delete respPack;
      }
      // MATCHMAKING QUEUE JOIN PACKET
      // MATCHMAKING QUEUE LEAVE PACKET
      else if (dynamic_cast<PacketMatchmakingLeaveRequest *>(pi) != nullptr)
      {
        std::cout << "Someone has left the matchmaking queue" << std::endl;
        PacketMatchmakingLeaveResponse *respPack = new PacketMatchmakingLeaveResponse();
        std::string jsonstring = respPack->toJson();
        socket().sendBytes(jsonstring.c_str(), jsonstring.size());
        delete respPack;
      }
      else if (dynamic_cast<PacketCellOpenRequest *>(pi) != nullptr)
      {
        PacketCellOpenRequest *x = dynamic_cast<PacketCellOpenRequest *>(pi);
        if (ActiveMatches::getInstance()->getMatchFromConnection(pc)->isRunning() == false) {
          delete ActiveMatches::getInstance()->getMatchFromConnection(pc);
        } else {
          ActiveMatches::getInstance()->getMatchFromConnection(pc)->playMove(pc, x->get_row(), x->get_column());
          
        }
      }
      else
      {
        std::cout << "Unknown packet type!" << std::endl;
      }
      delete pi;
      // MATCHMAKING QUEUE LEAVE PACKET
      lobbyState.print_connected_stats();
      buffer.clear();

      n = socket().receiveBytes(buffer.begin(), buffer.size());
    }
  }
};

class EchoConnectionFactory : public Poco::Net::TCPServerConnectionFactory
{
public:
  Poco::Net::TCPServerConnection *createConnection(const Poco::Net::StreamSocket &socket)
  {
    return new EchoConnection(socket);
  }
};

class EchoServer : public Poco::Util::ServerApplication
{
protected:
  int main(const std::vector<std::string> &args)
  {
    Poco::Net::ServerSocket svs(Poco::Net::SocketAddress("pairsweeper.fun", 2407));
    Poco::Net::TCPServerParams::Ptr pParams = new Poco::Net::TCPServerParams;
    pParams->setMaxQueued(100);
    pParams->setMaxThreads(16);
    Poco::Net::TCPServer srv(new EchoConnectionFactory, svs, pParams);
    srv.start();
    waitForTerminationRequest();
    srv.stop();
    return Application::EXIT_OK;
  }
};

POCO_SERVER_MAIN(EchoServer)
