#ifndef PACKET_MATCH_START_RESPONSE
#define PACKET_MATCH_START_RESPONSE

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#include "packet_response.hpp"
#include <iostream>

class PacketMatchStartResponse : public PacketResponse {
private:
  std::string _opponentName;
public:
  std::string get_opponent_name();

  friend std::ostream& operator<< (std::ostream& stream, const PacketMatchStartResponse* obj);
  PacketMatchStartResponse(std::string opponentName);
  std::string toJson();
  static PacketMatchStartResponse* fromJson(const std::string& json);
};

#endif
