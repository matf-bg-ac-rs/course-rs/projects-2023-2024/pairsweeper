
#include "packet_match_state_response.hpp"
#include <iostream>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketMatchStateResponse::PacketMatchStateResponse(std::string strCellBoard, int ownPoints, int opponentPoints)
  : PacketResponse("match_state_response"),
  _strCellBoard(strCellBoard),
  _ownPoints(ownPoints),
  _opponentPoints(opponentPoints) {};

int PacketMatchStateResponse::get_own_points() { return _ownPoints; }
int PacketMatchStateResponse::get_opponent_points() { return _opponentPoints; }
std::string PacketMatchStateResponse::get_str_cell_board() { return _strCellBoard; }

std::ostream& operator<<(std::ostream& stream, const PacketMatchStateResponse* obj) {
  stream << PBLUE << obj-> type << PCLR << "(" << obj->_strCellBoard << "), (" << obj->_ownPoints <<":" << obj->_opponentPoints << ")";
  return stream;
}

std::string PacketMatchStateResponse::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  obj.set("strCellBoard", _strCellBoard);
  obj.set("ownPoints", _ownPoints);
  obj.set("opponentPoints", _opponentPoints);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketMatchStateResponse* PacketMatchStateResponse::fromJson(const std::string& json) {
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(json);
  Poco::JSON::Object::Ptr pObject = result.extract<Poco::JSON::Object::Ptr>();

  std::string strCellBoard = pObject->getValue<std::string>("strCellBoard");
  int ownPoints = pObject->getValue<int>("ownPoints");
  int opponentPoints = pObject->getValue<int>("opponentPoints");
  
  return new PacketMatchStateResponse(strCellBoard, ownPoints, opponentPoints);
}

