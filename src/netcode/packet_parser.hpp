#ifndef PACKET_PARSER_HPP
#define PACKET_PARSER_HPP

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Stringifier.h>

#include "packet_request.hpp"
#include "packet_response.hpp"

class PacketParser {

public:
  static PacketRequest* parse_inbound_packet(const std::string& jsonstring);
  static PacketResponse* parse_response_packet(const std::string& jsonstring);
};

#endif
