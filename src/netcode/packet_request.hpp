#ifndef PACKET_INBOUND_HPP
#define PACKET_INBOUND_HPP

#include "packet.hpp"

class PacketRequest : public Packet {
public:
  PacketRequest(const std::string& type);
};

#endif
