#include "packet_matchmaking_join_response.hpp"
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketMatchmakingJoinResponse::PacketMatchmakingJoinResponse()
  : PacketResponse("matchmaking_join_response") {}


std::ostream& operator<<(std::ostream& stream, const PacketMatchmakingJoinResponse* obj) {
  stream << PBLUE << obj->getType() << PCLR << "(" <<  ")";
  return stream;
}

std::string PacketMatchmakingJoinResponse::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketMatchmakingJoinResponse* PacketMatchmakingJoinResponse::fromJson(const std::string& json) {
  return new PacketMatchmakingJoinResponse();
}
