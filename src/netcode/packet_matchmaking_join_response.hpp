#ifndef PACKET_MATCHMAKING_JOIN_RESPONSE
#define PACKET_MATCHMAKING_JOIN_RESPONSE

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#include "packet_response.hpp"
#include <iostream>

class PacketMatchmakingJoinResponse : public PacketResponse {
private:
public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketMatchmakingJoinResponse* obj);
  PacketMatchmakingJoinResponse();
  std::string toJson();
  static PacketMatchmakingJoinResponse* fromJson(const std::string& json);
};

#endif
