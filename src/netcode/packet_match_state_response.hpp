
#ifndef PACKET_MATCH_STATE_RESPONSE_HPP
#define PACKET_MATCH_STATE_RESPONSE_HPP

#include "packet_response.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

class PacketMatchStateResponse : public PacketResponse {
private:
  std::string _strCellBoard;
  int _ownPoints;
  int _opponentPoints;

public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketMatchStateResponse* obj);
  PacketMatchStateResponse(std::string strCellBoard, int ownPoints, int opponentPoints);
  int get_own_points();
  int get_opponent_points();
  std::string get_str_cell_board();

  std::string toJson();
  static PacketMatchStateResponse* fromJson(const std::string& json);
};

#endif
