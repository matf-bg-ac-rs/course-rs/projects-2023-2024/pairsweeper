#ifndef PACKET_LOGIN_RESPONSE_HPP
#define PACKET_LOGIN_RESPONSE_HPP
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#include "packet_response.hpp"
#include <iostream>

class PacketLoginResponse : public PacketResponse {
private:
public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketLoginResponse* obj);
  PacketLoginResponse();
  std::string toJson();
  static PacketLoginResponse* fromJson(const std::string& json);

};

#endif
