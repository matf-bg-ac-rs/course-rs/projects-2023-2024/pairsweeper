#include "packet_match_start_response.hpp"
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketMatchStartResponse::PacketMatchStartResponse(std::string opponentName)
  : PacketResponse("match_start_response"),
    _opponentName(opponentName) {};


std::string PacketMatchStartResponse::get_opponent_name() { return _opponentName;}


std::ostream& operator<<(std::ostream& stream, const PacketMatchStartResponse* obj) {
  stream << PBLUE << obj->getType() << PCLR << "(" <<  ")";
  return stream;
}

std::string PacketMatchStartResponse::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  obj.set("opponentName", _opponentName);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketMatchStartResponse* PacketMatchStartResponse::fromJson(const std::string& json) {
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(json);
  Poco::JSON::Object::Ptr pObject = result.extract<Poco::JSON::Object::Ptr>();
    std::string strOpponentName = pObject->getValue<std::string>("opponentName");

  return new PacketMatchStartResponse(strOpponentName);
}