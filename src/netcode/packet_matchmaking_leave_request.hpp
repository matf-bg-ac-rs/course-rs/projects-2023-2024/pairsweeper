#ifndef PACKET_MATCHMAKING_LEAVE_REQUEST
#define PACKET_MATCHMAKING_LEAVE_REQUEST

#include "packet_request.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

class PacketMatchmakingLeaveRequest : public PacketRequest {
private:
public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketMatchmakingLeaveRequest* obj);
  PacketMatchmakingLeaveRequest();

  std::string toJson();
  static PacketMatchmakingLeaveRequest* fromJson(const std::string& json);
};

#endif
