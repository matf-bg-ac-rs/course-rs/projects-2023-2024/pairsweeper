#include "packet_cell_open_request.hpp"
#include <iostream>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketCellOpenRequest::PacketCellOpenRequest(int row, int column)
  : PacketRequest("cell_open_request"),
  _row(row),
  _column(column) {};

int PacketCellOpenRequest::get_row() { return _row; }
int PacketCellOpenRequest::get_column() { return _column; }

std::ostream& operator<<(std::ostream& stream, const PacketCellOpenRequest* obj) {
  stream << PBLUE << obj->_column << PCLR << "(" << obj->_row << ")";
  return stream;
}

std::string PacketCellOpenRequest::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  obj.set("row", _row);
  obj.set("column", _column);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketCellOpenRequest* PacketCellOpenRequest::fromJson(const std::string& json) {
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(json);
  Poco::JSON::Object::Ptr pObject = result.extract<Poco::JSON::Object::Ptr>();

  std::string row = pObject->getValue<std::string>("row");
  std::string column = pObject->getValue<std::string>("column");
  
    int iRow = std::stoi(row);
    int iColumn = std::stoi(column);

  return new PacketCellOpenRequest(iRow, iColumn);
}

