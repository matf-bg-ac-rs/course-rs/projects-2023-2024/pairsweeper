#include "packet_login_request.hpp"
#include <iostream>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketLoginRequest::PacketLoginRequest(const std::string& username, const std::string& hash, const std::string& version)
  : PacketRequest("login_request"),
  _username(username),
  _hash(hash),
  _version(version) {}

const std::string PacketLoginRequest::get_username() { return _username; }
const std::string PacketLoginRequest::getHash() { return _hash; }
const std::string PacketLoginRequest::getVersion() { return _version; }

std::ostream& operator<<(std::ostream& stream, const PacketLoginRequest* obj) {
  stream << PBLUE << obj->getType() << PCLR << "(" << obj->_username << " : " << obj->_hash << " : " << obj->_version << ")";
  return stream;
}

std::string PacketLoginRequest::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  obj.set("username", _username);
  obj.set("hash", _hash);
  obj.set("version", _version);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketLoginRequest* PacketLoginRequest::fromJson(const std::string& json) {
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(json);
  Poco::JSON::Object::Ptr pObject = result.extract<Poco::JSON::Object::Ptr>();

  std::string username = pObject->getValue<std::string>("username");
  std::string hash = pObject->getValue<std::string>("hash");
  std::string version = pObject->getValue<std::string>("version");
  
  return new PacketLoginRequest(username, hash, version);
}

