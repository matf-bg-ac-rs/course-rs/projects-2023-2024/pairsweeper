#include "packet_parser.hpp"
#include "packet_login_request.hpp"
#include "packet_login_response.hpp"
#include "packet_matchmaking_join_request.hpp"
#include "packet_matchmaking_join_response.hpp"
#include "packet_matchmaking_leave_request.hpp"
#include "packet_matchmaking_leave_response.hpp"
#include "packet_match_start_response.hpp"
#include "packet_cell_open_request.hpp"
#include "packet_match_end_response.hpp"
#include "packet_match_state_response.hpp"
#include <Poco/JSON/Parser.h>
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Array.h>

PacketRequest* PacketParser::parse_inbound_packet(const std::string& jsonstring) {
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(jsonstring);
  Poco::JSON::Object::Ptr pObject = result.extract<Poco::JSON::Object::Ptr>();
  std::string property = pObject->getValue<std::string>("type");

  std::cout << "parsing type:" << property << std::endl;

  if (property == "login_request") {
    return PacketLoginRequest::fromJson(jsonstring);
  }
  if (property == "matchmaking_join_request") {
    return PacketMatchmakingJoinRequest::fromJson(jsonstring);
  }
  if (property == "matchmaking_leave_request") {
    return PacketMatchmakingLeaveRequest::fromJson(jsonstring);
  }
  if (property == "cell_open_request") {
    return PacketCellOpenRequest::fromJson(jsonstring);
  }

  return nullptr;
} 

PacketResponse* PacketParser::parse_response_packet(const std::string& jsonstring) {

  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(jsonstring);
  Poco::JSON::Object::Ptr pObject = result.extract<Poco::JSON::Object::Ptr>();
  std::string property = pObject->getValue<std::string>("type");
  std::cout << "parsing type:" << property << std::endl;

  if (property == "login_response") {
    return PacketLoginResponse::fromJson(jsonstring);
  }
  if (property == "matchmaking_join_response") {
    return PacketMatchmakingJoinResponse::fromJson(jsonstring);
  }
  if (property == "matchmaking_leave_response") {
    return PacketMatchmakingLeaveResponse::fromJson(jsonstring);
  }
  if (property == "match_start_response") {
    return PacketMatchStartResponse::fromJson(jsonstring);
  }
  if (property == "match_state_response") {
    return PacketMatchStateResponse::fromJson(jsonstring);
  }
  if (property == "match_end_response") {
    return PacketMatchEndResponse::fromJson(jsonstring);
  }

  return nullptr;
}

