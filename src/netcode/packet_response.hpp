#ifndef PACKET_OUTBOUND_HPP
#define PACKET_OUTBOUND_HPP

#include "packet.hpp"

class PacketResponse : public Packet {
public:
  PacketResponse(const std::string& type);
};

#endif
