#include "packet_matchmaking_leave_response.hpp"
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketMatchmakingLeaveResponse::PacketMatchmakingLeaveResponse()
  : PacketResponse("matchmaking_leave_response") {}


std::ostream& operator<<(std::ostream& stream, const PacketMatchmakingLeaveResponse* obj) {
  stream << PBLUE << obj->getType() << PCLR << "(" <<  ")";
  return stream;
}

std::string PacketMatchmakingLeaveResponse::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketMatchmakingLeaveResponse* PacketMatchmakingLeaveResponse::fromJson(const std::string& json) {
  return new PacketMatchmakingLeaveResponse();
}