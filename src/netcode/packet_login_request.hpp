#ifndef PACKET_LOGIN_REQUEST_HPP
#define PACKET_LOGIN_REQUEST_HPP

#include "packet_request.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

class PacketLoginRequest : public PacketRequest {
private:
  const std::string _username;
  const std::string _hash;
  const std::string _version;
public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketLoginRequest* obj);
  PacketLoginRequest(const std::string& username, const std::string& hash, const std::string& version);
  const std::string get_username();
  const std::string getHash();
  const std::string getVersion();

  std::string toJson();
  static PacketLoginRequest* fromJson(const std::string& json);
};

#endif

