#include "packet_login_response.hpp"
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketLoginResponse::PacketLoginResponse()
  : PacketResponse("login_response") {}


std::ostream& operator<<(std::ostream& stream, const PacketLoginResponse* obj) {
  stream << PBLUE << obj->getType() << PCLR << "(" <<  ")";
  return stream;
}

std::string PacketLoginResponse::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketLoginResponse* PacketLoginResponse::fromJson(const std::string& json) {
  return new PacketLoginResponse();
}
