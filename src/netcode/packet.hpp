#ifndef PACKET_HPP
#define PACKET_HPP

#include <string>

class Packet {
protected:
  const std::string type;
public:
  Packet(const std::string& type);
  virtual ~Packet() = default;
  virtual std::string getType() const;
};

#endif
