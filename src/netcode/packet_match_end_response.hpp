
#ifndef PACKET_MATCH_END_RESPONSE_HPP
#define PACKET_MATCH_END_RESPONSE_HPP

#include "packet_response.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

class PacketMatchEndResponse : public PacketResponse {
private:
  std::string _winner;
  int _ownPoints;
  int _opponentPoints;

public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketMatchEndResponse* obj);
  PacketMatchEndResponse(std::string strCellBoard, int ownPoints, int opponentPoints);
  int get_own_points();
  int get_opponent_points();
  std::string get_winner();

  std::string toJson();
  static PacketMatchEndResponse* fromJson(const std::string& json);
};

#endif
