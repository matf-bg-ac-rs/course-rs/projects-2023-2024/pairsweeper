#ifndef PACKET_CELL_OPEN_REQUEST_HPP
#define PACKET_CELL_OPEN_REQUEST_HPP

#include "packet_request.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

class PacketCellOpenRequest : public PacketRequest {
private:
  int _row;
  int _column;
public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketCellOpenRequest* obj);
  PacketCellOpenRequest(int row, int column);
  int get_row();
  int get_column();

  std::string toJson();
  static PacketCellOpenRequest* fromJson(const std::string& json);
};

#endif
