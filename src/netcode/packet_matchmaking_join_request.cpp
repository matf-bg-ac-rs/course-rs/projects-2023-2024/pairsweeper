#include "packet_matchmaking_join_request.hpp"
#include <iostream>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketMatchmakingJoinRequest::PacketMatchmakingJoinRequest()
  : PacketRequest("matchmaking_join_request") {}

std::ostream& operator<<(std::ostream& stream, const PacketMatchmakingJoinRequest* obj) {
  stream << PBLUE << obj->getType() << PCLR << "()";
  return stream;
}

std::string PacketMatchmakingJoinRequest::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketMatchmakingJoinRequest* PacketMatchmakingJoinRequest::fromJson(const std::string& json) {
  return new PacketMatchmakingJoinRequest();
}
