#include "packet_matchmaking_leave_request.hpp"
#include <iostream>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketMatchmakingLeaveRequest::PacketMatchmakingLeaveRequest()
  : PacketRequest("matchmaking_leave_request") {}

std::ostream& operator<<(std::ostream& stream, const PacketMatchmakingLeaveRequest* obj) {
  stream << PBLUE << obj->getType() << PCLR << "()";
  return stream;
}

std::string PacketMatchmakingLeaveRequest::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketMatchmakingLeaveRequest* PacketMatchmakingLeaveRequest::fromJson(const std::string& json) {
  return new PacketMatchmakingLeaveRequest();
}

