
#include "packet_match_end_response.hpp"
#include <iostream>

#define PBLUE "\u001b[34m"
#define PCLR  "\u001b[0m"

PacketMatchEndResponse::PacketMatchEndResponse(std::string winner, int ownPoints, int opponentPoints)
  : PacketResponse("match_end_response"),
  _winner(winner),
  _ownPoints(ownPoints),
  _opponentPoints(opponentPoints) {};

int PacketMatchEndResponse::get_own_points() { return _ownPoints; }
int PacketMatchEndResponse::get_opponent_points() { return _opponentPoints; }
std::string PacketMatchEndResponse::get_winner() { return _winner; }

std::ostream& operator<<(std::ostream& stream, const PacketMatchEndResponse* obj) {
  stream << PBLUE << obj-> type << PCLR << "(" << obj->_winner << "), (" << obj->_ownPoints <<":" << obj->_opponentPoints << ")";
  return stream;
}

std::string PacketMatchEndResponse::toJson() {
  Poco::JSON::Object obj;
  obj.set("type", type);
  obj.set("winner", _winner);
  obj.set("ownPoints", _ownPoints);
  obj.set("opponentPoints", _opponentPoints);
  std::ostringstream ostr;
  obj.stringify(ostr);
  return ostr.str();
}

PacketMatchEndResponse* PacketMatchEndResponse::fromJson(const std::string& json) {
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var result = parser.parse(json);
  Poco::JSON::Object::Ptr pObject = result.extract<Poco::JSON::Object::Ptr>();

    std::string winner = pObject->getValue<std::string>("winner");
  std::string strOwnPoints = pObject->getValue<std::string>("ownPoints");
  std::string strOpponentPoints = pObject->getValue<std::string>("opponentPoints");
  
    int ownPoints = std::stoi(strOwnPoints);
    int opponentPoints = std::stoi(strOpponentPoints);

  return new PacketMatchEndResponse(winner, ownPoints, opponentPoints);
}

