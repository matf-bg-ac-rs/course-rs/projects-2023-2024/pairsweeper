#ifndef PACKET_MATCHMAKING_LEAVE_RESPONSE_HPP
#define PACKET_MATCHMAKING_LEAVE_RESPONSE_HPP

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

#include "packet_response.hpp"
#include <iostream>

class PacketMatchmakingLeaveResponse : public PacketResponse {
private:
public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketMatchmakingLeaveResponse* obj);
  PacketMatchmakingLeaveResponse();
  std::string toJson();
  static PacketMatchmakingLeaveResponse* fromJson(const std::string& json);
};

#endif
