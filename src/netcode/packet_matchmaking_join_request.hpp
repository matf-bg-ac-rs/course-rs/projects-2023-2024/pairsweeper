#ifndef PACKET_MATCHMAKING_JOIN_REQUEST
#define PACKET_MATCHMAKING_JOIN_REQUEST

#include "packet_request.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>

class PacketMatchmakingJoinRequest : public PacketRequest {
private:
public:
  friend std::ostream& operator<< (std::ostream& stream, const PacketMatchmakingJoinRequest* obj);
  PacketMatchmakingJoinRequest();

  std::string toJson();
  static PacketMatchmakingJoinRequest* fromJson(const std::string& json);
};

#endif
