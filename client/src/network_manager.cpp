#include "network_manager.hpp"
#include <QDebug>
#include <QtCore>
#include "../../src/netcode/packet_parser.hpp"
#include "../../src/netcode/packet_matchmaking_join_response.hpp"
#include "../../src/netcode/packet_matchmaking_leave_response.hpp"
#include "../../src/netcode/packet_login_response.hpp"
#include "../../src/netcode/packet_match_start_response.hpp"
#include "../../src/netcode/packet_match_end_response.hpp"

NetworkManager *NetworkManager::instance = 0;

NetworkManager &NetworkManager::getInstance()
{
   if (!instance)
   {
      instance = new NetworkManager();
   }
   return *instance;
}

void NetworkManager::startConnection(std::string host, int port)
{
   Poco::Net::SocketAddress sa(host, port);
   socket.connect(sa);
   running = true;
}

void NetworkManager::stopConnection()
{
   socket.close();
   running = false;
}

void NetworkManager::sendPacket(const std::string &data)
{
   if (running)
   {
      socket.sendBytes(data.c_str(), data.size());
   }
}

void NetworkManager::run()
{
   while (running)
   {
      Poco::Buffer<char> buffer(1024);

      int n;
      n = socket.receiveBytes(buffer.begin(), buffer.size());
      while (n > 0)
      {
         std::string receivedData(buffer.begin(), buffer.begin() + n);
         PacketResponse* pr = PacketParser::parse_response_packet(receivedData);
         std::cout << receivedData << std::endl;
         if (dynamic_cast<PacketLoginResponse*>(pr) != nullptr) {
            std::cout << "Login response received" << std::endl;
            delete pr;
         }
         else if (dynamic_cast<PacketMatchmakingJoinResponse*>(pr) != nullptr) {
            emit onMatchmakingJoin();
            delete pr;
         }
         else if (dynamic_cast<PacketMatchmakingLeaveResponse*>(pr) != nullptr) {
            emit onMatchmakingLeave();
            delete pr;

         } 
         else if (dynamic_cast<PacketMatchStartResponse*>(pr) != nullptr) {
            emit onMatchStart();
            delete pr;
         } 
         else if (dynamic_cast<PacketMatchStateResponse*>(pr) != nullptr) {
            PacketMatchStateResponse* packetMatchStateResponse = 
               dynamic_cast<PacketMatchStateResponse*>(pr);
            emit onMatchState(packetMatchStateResponse);
         } 
         else if (dynamic_cast<PacketMatchEndResponse*>(pr) != nullptr) {
            PacketMatchEndResponse* packetMatchEndResponse = 
               dynamic_cast<PacketMatchEndResponse*>(pr);
            emit onMatchEnd(packetMatchEndResponse);
            this->stopConnection();
            QCoreApplication::quit();
         } 
          else {
            std::cout << "Unknown packet received" << std::endl;
            exit(EXIT_FAILURE);
         }

         buffer.clear();
         n = socket.receiveBytes(buffer.begin(), buffer.size());
      }
      qDebug() << "Terminated NetworkManager";
      
   }
}