#include "signalbarspinner.hpp"
#include "animatedbar.hpp"

SignalBarSpinner::SignalBarSpinner(QWidget *parent) : QGraphicsView(parent) {
   this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   scene = new QGraphicsScene(this);
   
   setScene(scene);

   bar1 = scene->addRect(0, 0, 10, 50);
   bar2 = scene->addRect(0, 0, 10, 50);
   bar3 = scene->addRect(0, 0, 10, 50);

    bar1->setBrush(QBrush(QColor("#FFFFFF"))); // Red
    bar2->setBrush(QBrush(QColor("#FFFFFF"))); // Green
    bar3->setBrush(QBrush(QColor("#FFFFFF"))); // Blue


   bar1->setPos(0, 0);
   bar2->setPos(20, 0);
   bar3->setPos(40, 0);

  AnimatedBar* animatedBar1 = new AnimatedBar(bar1);
  AnimatedBar* animatedBar2 = new AnimatedBar(bar2);
  AnimatedBar* animatedBar3 = new AnimatedBar(bar3);

  animation1 = new QPropertyAnimation(animatedBar1, "y");
  animation2 = new QPropertyAnimation(animatedBar2, "y");
  animation3 = new QPropertyAnimation(animatedBar3, "y");

   animation1->setDuration(1200);
   animation2->setDuration(1200);
   animation3->setDuration(1200);

   animation1->setStartValue(100);
   animation2->setStartValue(200);
   animation3->setStartValue(300);

   animation1->setEndValue(-150);
   animation2->setEndValue(-200);
   animation3->setEndValue(-250);

   animation1->setLoopCount(-1);
   animation2->setLoopCount(-1);
   animation3->setLoopCount(-1);

   connect(animation1, &QPropertyAnimation::finished, [=](){
       bar1->setY(0);
   });

   connect(animation2, &QPropertyAnimation::finished, [=](){
       bar2->setY(50);
   });

   connect(animation3, &QPropertyAnimation::finished, [=](){
       bar3->setY(100);
   });

   timer = new QTimer(this);

   connect(timer, &QTimer::timeout, [=](){
      animation1->start();
      animation2->start();
      animation3->start();
   });

   QTimer::singleShot(0, timer, [=](){
          animation1->start();
      animation2->start();
      animation3->start();
   });

   timer->start(1000);
}
