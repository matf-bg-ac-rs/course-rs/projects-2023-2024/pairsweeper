#include "playerdata.hpp"

PlayerData& PlayerData::getInstance() {
   static PlayerData instance;
   return instance;
}

void PlayerData::setUsername(std::string name) {
   username = name;
}

std::string PlayerData::getUsername() {
   return username;
}
