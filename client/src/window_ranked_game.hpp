#ifndef WINDOW_RANKED_GAME_HPP
#define WINDOW_RANKED_GAME_HPP

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include "network_manager.hpp" // Include the header file where NetworkClient is defined

class WindowRankedGame : public QWidget {
  Q_OBJECT

public:
  explicit WindowRankedGame(QWidget *parent = nullptr);

private slots:
  void onMatchState(PacketMatchStateResponse* packetMatchStateResponse);
  void onMatchEnd(PacketMatchEndResponse* packetMatchEndResponse);

private:
  QPushButton* cellButtons[14][14];
  QGridLayout *gameBoardLayout;

  QLabel *labelLowerLeft;
  QLabel *labelLowerRight;
};

#endif 
