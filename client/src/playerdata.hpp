#ifndef PLAYER_DATA_HPP
#define PLAYER_DATA_HPP

#include <string>

class PlayerData {
public:
   static PlayerData& getInstance();

   void setUsername(std::string name);
   std::string getUsername();

private:
   PlayerData() {}
   PlayerData(const PlayerData&) = delete;
   PlayerData& operator=(const PlayerData&) = delete;

   std::string username;
};

#endif // PLAYER_DATA_HPP