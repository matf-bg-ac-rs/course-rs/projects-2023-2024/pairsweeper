#ifndef SIGNALBARSPINNER_HPP
#define SIGNALBARSPINNER_HPP

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QPropertyAnimation>
#include <QTimer>

class SignalBarSpinner : public QGraphicsView {
public:
   SignalBarSpinner(QWidget *parent = nullptr);

private:
   QGraphicsScene* scene;
   QGraphicsRectItem* bar1;
   QGraphicsRectItem* bar2;
   QGraphicsRectItem* bar3;

   QPropertyAnimation* animation1;
   QPropertyAnimation* animation2;
   QPropertyAnimation* animation3;

   QTimer* timer;
};

#endif // SIGNALBARSPINNER_HPP
