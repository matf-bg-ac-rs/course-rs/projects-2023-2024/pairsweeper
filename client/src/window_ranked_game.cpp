#include "window_ranked_game.hpp"
#include "window_home.hpp"
#include "network_manager.hpp"
#include "../../src/netcode/packet_cell_open_request.hpp"
#include "playerdata.hpp"
#include <QDebug>
#include <QPushButton>
#include <QGridLayout>
#include <string>
#include <QIcon>
#include <iostream>
#include <QString>
#include <QTimer>

WindowRankedGame::WindowRankedGame(QWidget *parent) : QWidget(parent)
{
  labelLowerLeft = new QLabel();
  labelLowerRight = new QLabel();
  connect(&NetworkManager::getInstance(), &NetworkManager::onMatchState, this, &WindowRankedGame::onMatchState);
  connect(&NetworkManager::getInstance(), &NetworkManager::onMatchEnd, this, &WindowRankedGame::onMatchEnd);

  gameBoardLayout = new QGridLayout;

  // icons
  QIcon unopenedIcon(":/images/unopened.svg");
  QPixmap unopenedPixmap = unopenedIcon.pixmap(QSize(128, 128));
  QIcon unopenedFixedSizeIcon(unopenedPixmap);

  for (int i = 0; i < 14; ++i)
  {
    for (int j = 0; j < 14; ++j)
    {
      cellButtons[i][j] = new QPushButton(this);
      cellButtons[i][j]->setFixedSize(32, 32);
      cellButtons[i][j]->setObjectName("UnopenedCell");

      connect(cellButtons[i][j], &QPushButton::clicked, this, [=]()
              {
            repaint();
            PacketCellOpenRequest* packCellOpenRequest =
            new PacketCellOpenRequest(i, j); 
              NetworkManager::getInstance().sendPacket(packCellOpenRequest->toJson());
              delete packCellOpenRequest;
               });
      gameBoardLayout->addWidget(cellButtons[i][j], i, j);
    }
  }

  gameBoardLayout->setSpacing(0);

  QHBoxLayout *scoreLayout = new QHBoxLayout;

  scoreLayout->addWidget(labelLowerLeft);
  scoreLayout->addStretch();
  scoreLayout->addWidget(labelLowerRight);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addLayout(gameBoardLayout);
  mainLayout->addLayout(scoreLayout);

  this->setLayout(mainLayout);
  this->adjustSize();
}

void WindowRankedGame::onMatchState(PacketMatchStateResponse *packetMatchStateResponse)
{

  int points = packetMatchStateResponse->get_own_points();
  std::string strPoints = std::to_string(points);
  points = packetMatchStateResponse->get_opponent_points();


  std::string opponentPoints = std::to_string(points);

  std::string strOwnLabel = "";
  std::string strOpponentLabel = "";
  strOwnLabel.append(PlayerData::getInstance().getUsername());
  strOwnLabel.append("[");
  strOwnLabel.append(strPoints);
  strOwnLabel.append("]");

  strOpponentLabel.append("Opponent");
  strOpponentLabel.append("[");
  strOpponentLabel.append(opponentPoints);
  strOpponentLabel.append("]");



  labelLowerLeft->setText(strOwnLabel.c_str());
  labelLowerRight->setText(strOpponentLabel.c_str());
  // ownScoreLabel->setText(QString(PlayerData::getInstance().getUsername().c_str()) +
  //  " : " +QString::number(packetMatchStateResponse->get_own_points()));
  //    opponentScoreLabel->setText("opponent" " : " + QString::number(packetMatchStateResponse->get_own_points()));


  std::string strCellBoard = packetMatchStateResponse->get_str_cell_board();
  for (int i = 0; i < 14; i++)
  {
    for (int j = 0; j < 14; j++)
    {
      for (int c = 0; c < 9; c++)
      {
        if (strCellBoard.at(i * 14 + j) == ('0' + c))
        {
          cellButtons[i][j]->setIcon(QIcon());
          cellButtons[i][j]->setText(QString((char)('0' + c)));
          cellButtons[i][j]->setObjectName("OpenedCell");
        }
      }
    }
  }

  delete packetMatchStateResponse;
}

void WindowRankedGame::onMatchEnd(PacketMatchEndResponse *packetMatchEndResponse)
{
  // HomeWindow* homeWindow = new HomeWindow;
  // homeWindow->show();
  QTimer::singleShot(0, this, &WindowRankedGame::deleteLater);
  delete packetMatchEndResponse;
}