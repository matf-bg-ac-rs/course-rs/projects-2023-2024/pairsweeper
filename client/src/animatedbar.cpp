#include "animatedbar.hpp"

AnimatedBar::AnimatedBar(QGraphicsRectItem* bar, QObject* parent) : QObject(parent), m_bar(bar) {}

qreal AnimatedBar::y() const {
   return m_bar->y();
}

void AnimatedBar::setY(qreal y) {
   m_bar->setY(y);
   emit yChanged();
}
