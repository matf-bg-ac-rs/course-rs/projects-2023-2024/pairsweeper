#include "window_login.hpp"
#include "window_home.hpp"
#include "network_manager.hpp"
#include "playerdata.hpp"
#include <QDebug>
#include <QTimer>
#include <QTimer>
LoginWindow::LoginWindow(QWidget *parent) : QWidget(parent) {

 QFormLayout *layout = new QFormLayout;

 QLabel *usernameLabel = new QLabel("username");
 QLabel *passwordLabel = new QLabel("password");
 QLabel *headerLabel = new QLabel("pairsweeper");
 headerLabel->setObjectName("HeaderLabel");

 usernameField = new QLineEdit;
 passwordField = new QLineEdit;

 usernameField->setToolTip("username");
 passwordField->setToolTip("password");

 usernameField->setWhatsThis("username");

 passwordField->setEchoMode(QLineEdit::Password);
 passwordField->setWhatsThis("password");

 QPushButton *submitButton = new QPushButton("Log in!");
 submitButton->setObjectName("LoginButton");

 layout->addWidget(headerLabel);
 layout->addWidget(usernameLabel);
 layout->addWidget(usernameField);
 layout->addWidget(passwordLabel);
 layout->addWidget(passwordField);
 layout->addWidget(submitButton);

 this->setLayout(layout);
 this->adjustSize();

 connect(submitButton, &QPushButton::clicked, this, &LoginWindow::onSubmitClicked);
}

void LoginWindow::onSubmitClicked() {
 qDebug() << "username:" << usernameField->text();
 qDebug() << "password:" << passwordField->text();

    std::string username = usernameField->text().toStdString();
    std::string password = passwordField->text().toStdString();

    PlayerData::getInstance().setUsername(username);

PacketLoginRequest* pi = new PacketLoginRequest(
    username,
    password,
    "infdev"
);

 NetworkManager::getInstance().sendPacket(pi->toJson());
 delete pi;
//  this->close();
 HomeWindow *homeWindow = new HomeWindow;
 homeWindow->show();
    QTimer::singleShot(0, this, &LoginWindow::deleteLater);
}
