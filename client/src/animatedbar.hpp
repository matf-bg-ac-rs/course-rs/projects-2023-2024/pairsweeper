#ifndef ANIMATEDBAR_HPP
#define ANIMATEDBAR_HPP

#include <QObject>
#include <QGraphicsRectItem>

class AnimatedBar : public QObject {
   Q_OBJECT
   Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)

public:
   explicit AnimatedBar(QGraphicsRectItem* bar, QObject* parent = nullptr);

   qreal y() const;
   void setY(qreal y);

signals:
   void yChanged();

private:
   QGraphicsRectItem* m_bar;
};

#endif // ANIMATEDBAR_HPP
