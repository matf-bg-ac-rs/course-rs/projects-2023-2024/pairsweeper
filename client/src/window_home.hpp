#ifndef HOMEWINDOW_HPP
#define HOMEWINDOW_HPP

#include <QWidget>
#include <QFormLayout>
#include <QPushButton>
#include <QLabel>

class HomeWindow : public QWidget {
 Q_OBJECT

private:
    void showMatchmakingWindow();
public:
 explicit HomeWindow(QWidget *parent = nullptr);

private slots:
    void onMultiplayerClicked();
};

#endif
