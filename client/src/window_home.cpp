#include "window_home.hpp"
#include "window_matchmaking.hpp"
#include "network_manager.hpp"
#include "../../src/netcode/packet_matchmaking_join_request.hpp"
#include <QTimer>
#include <QDebug>

HomeWindow::HomeWindow(QWidget *parent) : QWidget(parent)
{
  this->setWindowFlags(Qt::FramelessWindowHint);
  this->setObjectName("WindowHome");
  QFormLayout *layout = new QFormLayout;

  QLabel *homeHeaderLabel = new QLabel("Welcome back, player.");
  QPushButton *singleplayerButton = new QPushButton("Singleplayer");
  QPushButton *multiplayerButton = new QPushButton("Multiplayer");
  QPushButton *settingsButton = new QPushButton("Settings");
  QPushButton *quitButton = new QPushButton("Quit program");

  homeHeaderLabel->setObjectName("HomeHeaderLabel");
  singleplayerButton->setObjectName("HomeOptionButton");
  multiplayerButton->setObjectName("HomeOptionButton");
  settingsButton->setObjectName("HomeOptionButton");
  quitButton->setObjectName("HomeOptionButton");

  layout->addWidget(homeHeaderLabel);
  layout->addWidget(singleplayerButton);
  layout->addWidget(multiplayerButton);
  layout->addWidget(settingsButton);
  layout->addWidget(quitButton);

  this->setLayout(layout);
  this->adjustSize();

  connect(multiplayerButton, &QPushButton::clicked, this, &HomeWindow::onMultiplayerClicked);
  connect(&NetworkManager::getInstance(), &NetworkManager::onMatchmakingJoin, this, &HomeWindow::showMatchmakingWindow);
}

// MATCHMAKING REQUEST / RESPONSE
void HomeWindow::onMultiplayerClicked() {
  PacketMatchmakingJoinRequest* pi = new PacketMatchmakingJoinRequest();
  
  qDebug() << "Multiplayer Clicked";

  NetworkManager::getInstance().sendPacket(
      pi->toJson()
  );
  delete pi;
}

void HomeWindow::showMatchmakingWindow()
{
  MatchmakingWindow *matchmakingWindow = new MatchmakingWindow;
 matchmakingWindow->show();
  QTimer::singleShot(0, this, &HomeWindow::deleteLater);
}
