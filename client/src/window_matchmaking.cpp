#include "window_matchmaking.hpp"
#include "network_manager.hpp"
#include <QDebug>
#include <QGraphicsItem>
#include <QPropertyAnimation>
#include <QTimer>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QPropertyAnimation>
#include <QTimer>
#include "signalbarspinner.hpp"
#include "window_home.hpp"
#include "window_ranked_game.hpp"
#include <QTimer>

MatchmakingWindow::MatchmakingWindow(QWidget *parent) : QWidget(parent)
{
    QFormLayout *layout = new QFormLayout;

    QLabel *matchmakingLabel = new QLabel("matchmaking");
    matchmakingLabel->setObjectName("MatchmakingLabel");

    SignalBarSpinner *matchmakingSpinner = new SignalBarSpinner;

    layout->addWidget(matchmakingLabel);
    layout->addWidget(matchmakingSpinner);

    this->setLayout(layout);
    this->adjustSize();
    QObject::connect(&NetworkManager::getInstance(), &NetworkManager::onMatchmakingLeave, this, &MatchmakingWindow::showHomeWindow);
    QObject::connect(&NetworkManager::getInstance(), &NetworkManager::onMatchStart, this, &MatchmakingWindow::showWindowRankedGame);

}

void MatchmakingWindow::showHomeWindow()
{
    HomeWindow *homeWindow = new HomeWindow;
    homeWindow->show();
    QTimer::singleShot(0, this, &MatchmakingWindow::deleteLater);
}

void MatchmakingWindow::showWindowRankedGame()
{
    WindowRankedGame *windowRankedGame = new WindowRankedGame;
    windowRankedGame->show();
    // QTimer::singleShot(0, this, &MatchmakingWindow::deleteLater);
}
