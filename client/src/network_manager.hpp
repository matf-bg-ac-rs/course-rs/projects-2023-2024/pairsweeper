#ifndef NETWORK_MANAGER_HPP
#define NETWORK_MANAGER_HPP

#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Runnable.h"
#include <QObject>
#include "../../src/netcode/packet_match_state_response.hpp"
#include "../../src/netcode/packet_match_end_response.hpp"



class NetworkManager : public QObject, public Poco::Runnable {
   Q_OBJECT

private:
   static NetworkManager* instance;
   Poco::Net::StreamSocket socket;
   bool running;

   NetworkManager() : running(false) {}

public:
   static NetworkManager& getInstance();

   void startConnection(std::string host, int port);
   void stopConnection();
   void sendPacket(const std::string& data);
   virtual void run() override;


signals:
   void onMatchmakingJoin();
   void onMatchmakingLeave();
   void onMatchStart();
   void onMatchState(PacketMatchStateResponse* packetMatchStateResponse);
   void onMatchEnd(PacketMatchEndResponse* packetMatchEndResponse);
};

#endif
