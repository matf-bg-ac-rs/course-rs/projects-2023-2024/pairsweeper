#include <QApplication>
#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFontDatabase>
#include <QFile>
#include <QFormLayout>
#include <iostream>
#include <QApplication>
#include <QFontDatabase>
#include <QFile>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QFormLayout>
#include <Poco/Runnable.h>
#include <Poco/Thread.h>
#include "window_login.hpp"
#include "network_manager.hpp"

int main(int argc, char **argv) {

    NetworkManager& manager = NetworkManager::getInstance();
    manager.startConnection("0.0.0.0", 2407);
    Poco::Thread networkThread;
    networkThread.start(manager);

 QApplication app(argc, argv);
    
 QFontDatabase fontDatabase;
 fontDatabase.addApplicationFont(":/fonts/montserrat.ttf");
 fontDatabase.addApplicationFont(":/fonts/raleway.ttf");

 QFile file(":/styles/styles.qss");
 file.open(QFile::ReadOnly);
 QString styleSheet = QLatin1String(file.readAll());
 qApp->setStyleSheet(styleSheet);

 LoginWindow *loginWindow = new LoginWindow;
 loginWindow->show();

 return app.exec();
}
