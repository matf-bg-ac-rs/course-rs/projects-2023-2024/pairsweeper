#ifndef MATCHMAKINGWINDOW_HPP
#define MATCHMAKINGWINDOW_HPP

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include "../../src/netcode/packet_login_request.hpp"

class MatchmakingWindow : public QWidget {
 Q_OBJECT

private:
    void showHomeWindow();
    void showWindowRankedGame();

public:
 explicit MatchmakingWindow(QWidget *parent = nullptr);

private slots:
    
};

#endif
