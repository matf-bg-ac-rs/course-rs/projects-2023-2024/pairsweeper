#ifndef LOGINWINDOW_HPP
#define LOGINWINDOW_HPP

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include "../../src/netcode/packet_login_request.hpp"

class LoginWindow : public QWidget {
 Q_OBJECT

private:
 QLineEdit *usernameField;
 QLineEdit *passwordField;

public:
 explicit LoginWindow(QWidget *parent = nullptr);

private slots:
 void onSubmitClicked();
};

#endif
