TEMPLATE = app
TARGET = ../bin/client
INCLUDEPATH += .
QT += gui
QT += widgets
QT += svg


HEADERS +=  src/network_manager.hpp \
            src/window_home.hpp \
            src/window_login.hpp \
            src/window_matchmaking.hpp \
            src/signalbarspinner.hpp \
            src/animatedbar.hpp \
            src/window_ranked_game.hpp \
            src/playerdata.hpp \
            ../src/netcode/packet_match_end_response.hpp \
            ../src/netcode/packet_match_state_response.hpp \
            ../src/netcode/packet_cell_open_request.hpp \
            ../src/netcode/packet_match_start_response.hpp \
            ../src/netcode/packet_matchmaking_leave_request.hpp \
            ../src/netcode/packet_matchmaking_leave_response.hpp \
            ../src/netcode/packet_matchmaking_join_request.hpp \
            ../src/netcode/packet_matchmaking_join_response.hpp \
            ../src/netcode/packet_parser.hpp \
            ../src/netcode/packet_login_request.hpp \
            ../src/netcode/packet_login_response.hpp \
            ../src/netcode/packet_request.hpp \
            ../src/netcode/packet_response.hpp \
            ../src/netcode/packet.hpp

SOURCES += src/main.cpp \
           src/network_manager.cpp \
           src/window_home.cpp \
           src/window_login.cpp \
           src/window_matchmaking.cpp \
           src/signalbarspinner.cpp \
           src/animatedbar.cpp \
           src/window_ranked_game.cpp \
           src/playerdata.cpp \
            ../src/netcode/packet_match_end_response.cpp \
            ../src/netcode/packet_match_state_response.cpp \
            ../src/netcode/packet_cell_open_request.cpp \
            ../src/netcode/packet_match_start_response.cpp \
            ../src/netcode/packet_matchmaking_leave_request.cpp \
            ../src/netcode/packet_matchmaking_leave_response.cpp \
            ../src/netcode/packet_matchmaking_join_request.cpp \
            ../src/netcode/packet_parser.cpp \
            ../src/netcode/packet_matchmaking_join_response.cpp \
            ../src/netcode/packet_login_request.cpp \
            ../src/netcode/packet_login_response.cpp \
            ../src/netcode/packet_request.cpp \
            ../src/netcode/packet_response.cpp \
            ../src/netcode/packet.cpp


RESOURCES += resources/fonts/resources_fonts.qrc \
             resources/styles/resources_styles.qrc \
             resources/images/resources_images.qrc

LIBS += -lPocoUtil -lPocoNet -lPocoFoundation -lPocoXML -lPocoJSON
