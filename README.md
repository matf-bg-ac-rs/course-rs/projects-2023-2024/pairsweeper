# pairsweeper
## Synopsis



`pairsweeper` is a multiplayer minesweeper-like game designed to provide an engaging and interactive experience for players. The game features a registration system that allows users to create an account and log in, ensuring a personalized experience for each player.

The game includes a matchmaking system that pairs players together based on their skill level and preferences. This system uses a matchmaking rating system to ensure fair and balanced matches. The rating system is designed to update in real-time based on the player's performance, ensuring that the rating is always reflective of the player's current skill level.
***


## Issues

The `development` kanban board may be accessed [here](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/pairsweeper/-/boards).

## Dependencies
The following dependencies are required to build the project from source:
```
POCO
QT
```

## Building
The game/server may be built from source, or downloaded as binaries from `releases`.

```bash
make client
make server
```

After running the following commands, the resulting binaries will be located in the `bin` directory.

## Docker
### [TODO]

***
## Collaborators
| IDENT | Contributor profile | Role | Issues |  
| :-----------| :----------- | :----------- | -------------------: |  
| `mi19325`| [Dušan Mirčić](https://gitlab.com/duki1)    |      netcode     | [standby](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/pairsweeper/-/issues/?sort=created_date&state=opened&assignee_username%5B%5D=duki1&first_page_size=20) |  
| `mi19182`| [Luka Samardžić](https://gitlab.com/SamardzicLuka)   |    core   |               [standby](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/pairsweeper/-/issues/?sort=created_date&state=opened&assignee_username%5B%5D=SamardzicLuka&first_page_size=20) |  
| `mi19108`| [Ivana Pantić](https://gitlab.com/IvanaIP)   |    persistence   |               [standby](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/pairsweeper/-/issues/?sort=created_date&state=opened&assignee_username%5B%5D=IvanaIP&first_page_size=20) |
| `mi19255`| [Đorđe Aksentijević](https://gitlab.com/adjordje)   |    matchmaking   |               [standby](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/pairsweeper/-/issues/?sort=created_date&state=opened&assignee_username%5B%5D=adjordje&first_page_size=20) |
| `mi17227`| [Anita Jovanović](https://gitlab.com/anitajovanovic)   |  user interface   |               [standby](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/pairsweeper/-/issues/?sort=created_date&state=opened&assignee_username%5B%5D=anitajovanovic&first_page_size=20) |
***

more information on pairsweeper.fun

DEMO: http://pairsweeper.fun:8000/
