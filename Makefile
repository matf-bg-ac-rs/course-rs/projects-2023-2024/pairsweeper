CXX = g++
CXXFLAGS = -Wall -g -Wextra -Werror -Wno-unused -std=c++17
LDFLAGS = -lPocoUtil -lPocoNet -lPocoFoundation -lPocoXML -lPocoJSON
TARGET = pairsweeper
COMMON_DIR = src/common
SERVER_DIR = src/server
NETCODE_DIR = src/netcode
OBJ_DIR = obj
BIN_DIR = bin
TEST_DIR = test

COMMON_TEST_TARGET = common_tests
NETCODE_TEST_TARGET = netcode_tests
SERVER_RELEASE_TARGET = server
CLIENT_TARGET = client
CLIENT_DIR = client

NETCODE_OBJ = $(OBJ_DIR)/$(NETCODE_DIR)
COMMON_OBJ = $(OBJ_DIR)/$(COMMON_DIR)

NPROCS = $(nproc)
MAKEFLAGS += -j$(NPROCS)

all: common netcode server client

server: directories common $(BIN_DIR)/$(SERVER_RELEASE_TARGET)
common: directories $(BIN_DIR)/$(COMMON_TEST_TARGET)
netcode: directories $(BIN_DIR)/$(NETCODE_TEST_TARGET) $(OBJ_FILES)

directories:
	mkdir -p $(OBJ_DIR) $(BIN_DIR) $(NETCODE_OBJ) $(COMMON_OBJ)

# SERVER
$(BIN_DIR)/$(SERVER_RELEASE_TARGET): $(SERVER_DIR)/*.cpp $(NETCODE_DIR)/*.cpp $(NETCODE_DIR)/*.hpp $(COMMON_DIR)/*.cpp $(COMMON_DIR)/*.hpp  
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
# SERVER

# NETCODE
$(BIN_DIR)/$(NETCODE_TEST_TARGET): $(TEST_DIR)/$(NETCODE_TEST_TARGET).cpp $(NETCODE_DIR)/*.cpp $(NETCODE_DIR)/*.hpp 
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
# NETCODE

# COMMON
$(BIN_DIR)/$(COMMON_TEST_TARGET): $(TEST_DIR)/$(COMMON_TEST_TARGET).cpp $(COMMON_DIR)/*.cpp $(COMMON_DIR)/*.hpp
	$(CXX) $(CXXFLAGS) -o $@ $^

%.o: $(COMMON_DIR)/%.cpp $(COMMON_DIR)/%.hpp
	$(CXX) $(CXXFLAGS) -c -o $(OBJ_DIR)/$@ $<
# COMMON

.PHONY: client

client:
	+cd $(CLIENT_DIR) && qmake && make

clean:
	rm -rf $(OBJ_DIR) $(BIN_DIR) && cd $(CLIENT_DIR) && make clean 

.NOTPARALLEL: client
